use super::*;
use crate::move_factories::MoveFactory;
use crate::scorers::Scorer;
use crate::specification::*;

pub struct ExhaustiveDescent<F, S> {
    pub move_factory: F,
    pub scorer: S,
    pub fixed: HashSet<usize>,
}

impl<F: MoveFactory, S: Scorer> Optimizer for ExhaustiveDescent<F, S> {
    fn optimize(
        &mut self,
        problem: &Problem,
        pose: &mut Pose,
        max_iterations: Option<usize>,
        cinema: &mut Cinema,
    ) {
        cinema.next_frame(pose.clone());

        let mut changed = true;
        let mut iterations = 0;
        let mut current_score = self.scorer.score(problem, pose);

        while changed
            && (max_iterations.is_none()
                || iterations < max_iterations.unwrap())
        {
            changed = false;
            iterations += 1;
            let mut chosen_move = None;
            for m in self.move_factory.get_moves(problem, &pose, &self.fixed) {
                m.apply(pose);
                let score = self.scorer.score(problem, &pose);
                m.revert(pose);
                if score < current_score {
                    current_score = score;
                    chosen_move = Some(m);
                }
            }
            if let Some(m) = chosen_move {
                m.apply(pose);
                cinema.next_frame(pose.clone());
                changed = true;
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::move_factories::VertexTranslations;
    use crate::scorers::Dislikes;

    #[test]
    fn following_reverse_dislikes() {
        let mut problem = Problem {
            hole: vec![[0, 0], [10, 0], [10, 10], [0, 10]],
            figure: Figure {
                vertices: vec![[1, 2], [11, -3], [13, 8], [-4, 12]],
                edges: vec![(0, 1), (1, 2), (2, 3), (3, 0)],
            },
            epsilon: 100_000,
            bonuses: vec![],
        };
        let mut pose = Pose {
            vertices: problem.figure.vertices.clone(),
            bonuses: Vec::new(),
        };
        ExhaustiveDescent {
            move_factory: VertexTranslations::new(&problem),
            scorer: Dislikes(),
            fixed: HashSet::new(),
        }
        .optimize(&problem, &mut pose, None, &mut Cinema(None));
        assert_eq!(pose.vertices.sort_unstable(), problem.hole.sort_unstable());
    }
}
