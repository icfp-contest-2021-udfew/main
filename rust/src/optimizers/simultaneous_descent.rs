use super::*;
use crate::move_factories::MoveFactory;

use crate::scorers::Scorer;
use crate::specification::*;

// use std::fs;
// use crate::movie::dump_movie;

pub struct SimultaneousDescent<F, S> {
    pub move_factory: F,
    pub scorer: S,
    pub fixed: HashSet<usize>,
}

impl<F: MoveFactory, S: Scorer> Optimizer for SimultaneousDescent<F, S> {
    fn optimize(
        &mut self,
        problem: &Problem,
        pose: &mut Pose,
        max_iterations: Option<usize>,
        cinema: &mut Cinema,
    ) {
        cinema.next_frame(pose.clone());

        let mut turn_counter = 0;
        let mut current_score = self.scorer.score(problem, pose);
        let mut moves_applied = -1;
        let mut iterations = 0;
        while moves_applied != 0
            && (max_iterations.is_none()
                || iterations < max_iterations.unwrap())
        {
            moves_applied = 0;
            iterations += 1;
            for m in self.move_factory.get_moves(problem, &pose, &self.fixed) {
                m.apply(pose);
                let mut score = current_score.clone();
                self.scorer.update(&mut score, &m, problem, &pose);
                if score >= current_score {
                    m.revert(pose);
                } else {
                    current_score = score;
                    moves_applied += 1;
                }
            }
            if moves_applied != 0 {
                eprintln!("{}: applied {} moves", turn_counter, moves_applied);
                turn_counter += 1;
                cinema.next_frame(pose.clone());
            }
        }
    }
}
