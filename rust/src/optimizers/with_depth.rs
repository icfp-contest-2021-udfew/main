use super::*;
use crate::move_factories::MoveFactory;
use crate::scorers::Scorer;
use crate::specification::*;

pub struct WithDepth<F, S>(pub ExhaustiveDescent<F, S>, pub u32);

impl<F: MoveFactory, S: Scorer> Optimizer for WithDepth<F, S> {
    fn optimize(
        &mut self,
        problem: &Problem,
        pose: &mut Pose,
        max_iterations: Option<usize>,
        cinema: &mut Cinema,
    ) {
        cinema.next_frame(pose.clone());

        let mut changed = true;
        let mut iterations = 0;
        // This will hopefully be enough to avoid reallocations...
        let mut poses = Vec::with_capacity(
            usize::pow(10, self.1 + 1) * pose.vertices.len(),
        );
        let mut current_score = self.0.scorer.score(problem, pose);

        let mut turn_counter = 0;
        while changed
            && (max_iterations.is_none()
                || iterations < max_iterations.unwrap())
        {
            changed = false;
            iterations += 1;
            poses.clear();
            poses.push((pose.clone(), current_score.clone()));
            let mut start_at = 0;
            let mut move_num = 0;
            for d in 0..self.1 {
                let poses_len = poses.len();
                for i in start_at..poses.len() {
                    let old = poses[i].clone();
                    for m in self.0.move_factory.get_moves(
                        problem,
                        &old.0,
                        &self.0.fixed,
                    ) {
                        let (mut new_pose, mut new_score) = old.clone();
                        m.apply(&mut new_pose);
                        self.0.scorer.update(
                            &mut new_score,
                            &m,
                            &problem,
                            &new_pose,
                        );

                        if new_score < current_score {
                            current_score = new_score.clone();
                            *pose = new_pose.clone();
                            changed = true;
                        }
                        if d + 1 < self.1 {
                            poses.push((new_pose, new_score));
                        }
                        move_num += 1;
                    }
                }
                start_at = poses_len;
            }
            eprintln!("{}: Looked at {} moves", turn_counter, move_num);
            turn_counter += 1;
            if changed {
                cinema.next_frame(pose.clone());
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::move_factories::VertexTranslations;
    use crate::scorers::Dislikes;

    #[test]
    fn following_reverse_dislikes() {
        let mut problem = Problem {
            hole: vec![[0, 0], [10, 0], [10, 10], [0, 10]],
            figure: Figure {
                vertices: vec![[1, 2], [11, -3], [13, 8], [-4, 12]],
                edges: vec![(0, 1), (1, 2), (2, 3), (3, 0)],
            },
            epsilon: 100_000,
            bonuses: vec![],
        };
        let mut pose = Pose {
            vertices: problem.figure.vertices.clone(),
            bonuses: Vec::new(),
        };
        WithDepth(
            ExhaustiveDescent {
                move_factory: VertexTranslations::new(&problem),
                scorer: Dislikes(),
                fixed: HashSet::new(),
            },
            3,
        )
        .optimize(&problem, &mut pose, None, &mut Cinema(None));
        assert_eq!(pose.vertices.sort_unstable(), problem.hole.sort_unstable());
    }
}
