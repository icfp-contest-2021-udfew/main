use rand::{rngs::ThreadRng, thread_rng, Rng};
use std::collections::BTreeMap;

use super::*;
use crate::checks::*;
use crate::geometry::clipping::{point_in_polygon, segment_in_polygon};
use crate::geometry::floating::*;
use crate::specification::*;

pub struct Annealer {
    pub friction_constant: Floating,
    pub spring_constant: Floating,
    pub wiggle_at: Floating,
    pub stop_at: Floating,
    pub rng: ThreadRng,
}

impl Annealer {
    pub fn new(
        friction_constant: Floating,
        spring_constant: Floating,
        wiggle_at: Floating,
        stop_at: Floating,
    ) -> Self {
        Annealer {
            friction_constant,
            spring_constant,
            wiggle_at,
            stop_at,
            rng: thread_rng(),
        }
    }
}

impl Optimizer for Annealer {
    fn optimize(
        &mut self,
        problem: &Problem,
        pose: &mut Pose,
        max_iterations: Option<usize>,
        cinema: &mut Cinema,
    ) {
        cinema.next_frame(pose.clone());

        let targets = {
            let mut map = BTreeMap::new();
            for (edge, broken) in pose.annotated_edges_for(problem) {
                let l = if broken {
                    let (k, l) = pose.broken_leg().unwrap();
                    squared_distance(
                        &problem.figure.vertices[k],
                        &problem.figure.vertices[l],
                    ) as Floating
                        / 2.0
                } else {
                    let (i, j) = edge;
                    squared_distance(
                        &problem.figure.vertices[i],
                        &problem.figure.vertices[j],
                    ) as Floating
                };
                map.insert(edge, l);
            }
            map
        };

        let mut vertices: Vec<[Floating; 2]> = pose
            .vertices
            .iter()
            .map(|p| ([p[0] as Floating, p[1] as Floating]))
            .collect();
        let mut speeds = vec![[0.0, 0.0]; vertices.len()];
        let mut accelerations = vec![[0.0, 0.0]; vertices.len()];

        let mut change = Floating::MAX;
        let mut iterations = 0;

        while change > self.stop_at
            && (max_iterations.is_none()
                || iterations < max_iterations.unwrap())
        {
            change = 0.0;
            iterations += 1;

            for a in accelerations.iter_mut() {
                *a = [0.0, 0.0];
            }

            let mut all_ok = true;
            for (edge, old_d, new_d) in pose.edges_with_distances_for(problem) {
                if problem.overstretch(old_d, new_d) > 0 {
                    all_ok = false;

                    let (i, j) = edge;
                    let start = vertices[i];
                    let end = vertices[j];

                    let vector = [end[0] - start[0], end[1] - start[1]];
                    let current_sq_d = sq_len(&vector);
                    let length = Floating::sqrt(current_sq_d);
                    let displacement = length - Floating::sqrt(targets[&edge]);

                    let acceleration = if length < self.wiggle_at {
                        let a = self.rng.gen_range(0.0..2.0 * PI);
                        let l = self.wiggle_at * self.rng.gen_range(1.0..1.5);
                        polar_to_cartesian(a, l)
                    } else {
                        [
                            self.spring_constant * displacement * vector[0]
                                / length,
                            self.spring_constant * displacement * vector[1]
                                / length,
                        ]
                    };

                    accelerations[i][0] += acceleration[0];
                    accelerations[i][1] += acceleration[1];
                    accelerations[j][0] -= acceleration[0];
                    accelerations[j][1] -= acceleration[1];
                }
            }

            if all_ok {
                break;
            }

            for (i, (s, a)) in
                speeds.iter_mut().zip(accelerations.iter()).enumerate()
            {
                s[0] = (s[0] + a[0]) * self.friction_constant;
                s[1] = (s[1] + a[1]) * self.friction_constant;
                let new_vertex = [vertices[i][0] + s[0], vertices[i][1] + s[1]];
                if !point_in_polygon(&round(&new_vertex), &problem.hole)
                    .in_closure()
                    || pose.edges_for(problem).any(|(vi, vj)| {
                        (vi == i
                            && !segment_in_polygon(
                                &(round(&new_vertex), round(&vertices[vj])),
                                &problem.hole,
                            ))
                            || (vj == i
                                && !segment_in_polygon(
                                    &(round(&new_vertex), round(&vertices[vi])),
                                    &problem.hole,
                                ))
                    })
                {
                    *s = [0., 0.];
                } else {
                    change =
                        Floating::max(change, d(&new_vertex, &vertices[i]));
                    vertices[i] = new_vertex;
                }
            }

            for (i, vertex) in vertices.iter().enumerate() {
                pose.vertices[i] = round(vertex);
            }

            cinema.next_frame(pose.clone());
            if iterations % 100 == 0 {
                eprintln!(
                    "Annealer iteration {}, change: {}",
                    iterations, change
                );
            }
        }
    }
}
