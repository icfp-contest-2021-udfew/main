pub mod annealer;
pub mod exhaustive_descent;
pub mod simultaneous_descent;
use std::collections::HashSet;
use std::fs;
use std::path::PathBuf;

pub use annealer::Annealer;
pub use exhaustive_descent::ExhaustiveDescent;
pub use simultaneous_descent::SimultaneousDescent;
pub mod with_depth;
pub use with_depth::WithDepth;

use crate::movie::dump_movie;
use crate::specification::*;

pub struct Cinema(pub Option<(PathBuf, Vec<Pose>, bool)>);

impl Cinema {
    pub fn next_frame(&mut self, pose: Pose) {
        match self.0.as_mut() {
            None => {}
            Some((_, v, l)) => {
                v.push(pose);
                if *l {
                    self.write()
                }
            }
        }
    }
    pub fn write(&self) {
        match self.0.as_ref() {
            None => {}
            Some((p, v, _)) => {
                fs::write(&p, dump_movie(&v)).unwrap();
            }
        }
    }
}

pub trait Optimizer {
    fn optimize(
        &mut self,
        _: &Problem,
        _: &mut Pose,
        _: Option<usize>,
        _: &mut Cinema,
    );
}

impl Optimizer for Vec<(Box<dyn Optimizer>, Option<usize>)> {
    fn optimize(
        &mut self,
        problem: &Problem,
        pose: &mut Pose,
        max_iterations: Option<usize>,
        cinema: &mut Cinema,
    ) {
        let mut current_pose = if max_iterations.is_some() {
            None
        } else {
            Some(pose.clone())
        };
        let mut iterations = 0;

        while max_iterations.is_none() || iterations < max_iterations.unwrap() {
            iterations += 1;

            for (o, its) in self.iter_mut() {
                o.optimize(problem, pose, *its, cinema)
            }

            if let Some(p) = current_pose.as_mut() {
                if *pose == *p {
                    break;
                } else {
                    *p = pose.clone();
                }
            }
        }
    }
}
