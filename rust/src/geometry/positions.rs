use std::collections::HashSet;

use super::*;
use crate::checks::STRETCH_DENOMINATOR;
use crate::graph::EdgeLenGraph;
use cached::proc_macro::cached;

#[cached]
fn possible_segment_vectors(sqlen: Scalar, epsilon: Scalar) -> HashSet<Point> {
    let epsilon = epsilon as f64 / STRETCH_DENOMINATOR as f64;
    let sqlen = sqlen as f64;
    let min_sqlen = (1. - epsilon) * sqlen;
    let max_sqlen = (1. + epsilon) * sqlen;

    let mut vectors_eighth = Vec::new();

    let mut y = 0;
    loop {
        let y2 = (y * y) as f64;
        let min_x = ((min_sqlen - y2).max(0.).sqrt().ceil() as Scalar).max(y);
        let max_x = (max_sqlen - y2).max(0.).sqrt().floor() as Scalar;

        if max_x < y {
            break;
        }

        for x in min_x..=max_x {
            vectors_eighth.push([x, y]);
        }

        y += 1;
    }

    let mut vectors = HashSet::with_capacity(vectors_eighth.len() * 8);

    for v in vectors_eighth {
        vectors.insert([v[0], v[1]]);
        vectors.insert([v[0], -v[1]]);
        vectors.insert([-v[0], v[1]]);
        vectors.insert([-v[0], -v[1]]);
        vectors.insert([v[1], v[0]]);
        vectors.insert([v[1], -v[0]]);
        vectors.insert([-v[1], v[0]]);
        vectors.insert([-v[1], -v[0]]);
    }

    vectors
}

pub fn possible_point_positions(
    constraints: &[(Point, Scalar)],
    epsilon: Scalar,
) -> HashSet<Point> {
    constraints
        .iter()
        .map(|(point, sqlen)| {
            possible_segment_vectors(*sqlen, epsilon)
                .iter()
                .map(|v| [point[0] + v[0], point[1] + v[1]])
                .collect::<HashSet<_>>()
        })
        .reduce(|a, b| &a & &b)
        .unwrap()
}

pub fn possible_vertex_positions(
    vertex: usize,
    pose: &Pose,
    graph: &EdgeLenGraph,
    epsilon: Scalar,
) -> HashSet<Point> {
    possible_point_positions(
        graph
            .neighbors(vertex.into())
            .map(|n| {
                (
                    pose.vertices[n.index()],
                    graph[graph.find_edge(vertex.into(), n).unwrap()],
                )
            })
            .collect::<Vec<_>>()
            .as_ref(),
        epsilon,
    )
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_possible_segment_vectors() {
        assert!(
            possible_segment_vectors(1, 1)
                == [[1, 0], [0, 1], [-1, 0], [0, -1]].iter().cloned().collect()
        );
    }

    #[test]
    fn test_possible_point_positions() {
        assert!(
            possible_point_positions(&[([0, 0], 1), ([2, 0], 1)], 1)
                == [[1, 0]].iter().cloned().collect()
        );
    }
}
