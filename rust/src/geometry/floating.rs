use std::f64::consts;

use crate::checks::squared_distance;
use crate::specification::*;

pub type Floating = f64;

pub static PI: Floating = consts::PI;

pub fn distance(p: &Point, q: &Point) -> Floating {
    Floating::sqrt(squared_distance(p, q) as Floating)
}

pub fn d(p: &[Floating; 2], q: &[Floating; 2]) -> Floating {
    Floating::sqrt(sq_d(p, q))
}

pub fn sq_d(p: &[Floating; 2], q: &[Floating; 2]) -> Floating {
    Floating::powi(q[0] - p[0], 2) + Floating::powi(q[1] - p[1], 2)
}

pub fn sq_len(p: &[Floating; 2]) -> Floating {
    Floating::powi(p[0], 2) + Floating::powi(p[1], 2)
}

pub fn round(p: &[Floating; 2]) -> Point {
    [
        Floating::round(p[0]) as Scalar,
        Floating::round(p[1]) as Scalar,
    ]
}

pub fn polar_to_cartesian(a: Floating, l: Floating) -> [Floating; 2] {
    [l * Floating::cos(a), l * Floating::sin(a)]
}
