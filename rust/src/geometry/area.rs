use super::Segment;
use crate::geometry::polygon_to_geo;
use crate::specification::*;
use geo;
use geo::algorithm::area::Area;
use geo_clipper::Clipper;
//use geo_booleanop::boolean::BooleanOp;

#[allow(clippy::ptr_arg)]
pub fn area_cut_out(seg: &Segment, polygon: &Polygon) -> f64 {
    let eps = 0.001;

    let seg = (
        [seg.0[0] as f64, seg.0[1] as f64],
        [seg.1[0] as f64, seg.1[1] as f64],
    );
    let v = [seg.1[0] - seg.0[0], seg.1[1] - seg.0[1]];

    let perturb0 = [-v[1] - v[0], v[0] - v[1]];
    let len0 = (perturb0[0].powf(2.) + perturb0[1].powf(2.)).sqrt();
    let perturb0 = [perturb0[0] / len0 * eps, perturb0[1] / len0 * eps];

    let perturb1 = [v[1] - v[0], -v[0] - v[1]];
    let len1 = (perturb1[0].powf(2.) + perturb1[1].powf(2.)).sqrt();
    let perturb1 = [perturb1[0] / len1 * eps, perturb1[1] / len1 * eps];

    let polygon = polygon_to_geo(&polygon);

    let thick_segment = geo::Polygon::new(
        geo::LineString::from(vec![
            (seg.1[0] - perturb0[0], seg.1[1] - perturb0[1]),
            (seg.1[0] - perturb1[0], seg.1[1] - perturb1[1]),
            (seg.0[0] + perturb0[0], seg.0[1] + perturb0[1]),
            (seg.0[0] + perturb1[0], seg.0[1] + perturb1[1]),
        ]),
        vec![],
    );

    //let union = polygon.union(&thick_segment);
    let union = polygon.union(&thick_segment, 1000. / eps);

    union
        .into_iter()
        .map(|p| p.into_inner().1)
        .flatten()
        .map(|int| geo::Polygon::new(int, vec![]).unsigned_area())
        .sum()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_area_cut_out() {
        let polygon = vec![
            [0, 0],
            [0, 4],
            [6, 4],
            [6, 0],
            [4, 0],
            [4, 2],
            [2, 2],
            [2, 0],
        ];

        let test_cases = vec![
            (([1, 1], [5, 1]), 2.),
            (([2, 0], [4, 2]), 2.),
            (([-1, 0], [7, 0]), 4.),
        ];

        for (seg, area) in test_cases {
            assert!((area_cut_out(&seg, &polygon) - area).abs() <= 0.01);
        }

        // let polygon = vec![
        //     [55, 80],
        //     [65, 95],
        //     [95, 95],
        //     [35, 5],
        //     [5, 5],
        //     [35, 50],
        //     [5, 95],
        //     [35, 95],
        //     [45, 80],
        // ];

        // println!("{}", area_cut_out(&([32, 41], [32, 71]), &polygon));

        // panic!()
    }
}
