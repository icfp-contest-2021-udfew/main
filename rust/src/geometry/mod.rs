pub mod area;
pub mod clipping;
pub mod distance;
pub mod floating;
pub mod positions;

use crate::specification::*;
use geo;

type Segment = (Point, Point);

pub struct EdgeIterator<'a> {
    polygon: &'a Polygon,
    index: usize,
}

impl<'a> Iterator for EdgeIterator<'a> {
    type Item = Segment;

    fn next(&mut self) -> Option<Self::Item> {
        if self.index == self.polygon.len() {
            return None;
        }

        let previous = if self.index == 0 {
            self.polygon.len() - 1
        } else {
            self.index - 1
        };

        self.index += 1;

        Some((self.polygon[previous], self.polygon[self.index - 1]))
    }
}

#[allow(clippy::ptr_arg)]
pub fn iter_edges(polygon: &Polygon) -> EdgeIterator {
    EdgeIterator { polygon, index: 0 }
}

#[allow(clippy::ptr_arg)]
fn polygon_to_geo(polygon: &Polygon) -> geo::Polygon<f64> {
    geo::Polygon::new(
        geo::LineString::from(
            polygon
                .iter()
                .map(|[x, y]| (*x as f64, *y as f64))
                .collect::<Vec<(f64, f64)>>(),
        ),
        vec![],
    )
}

fn point_to_geo(point: &Point) -> geo::Point<f64> {
    geo::Point::new(point[0] as f64, point[1] as f64)
}
