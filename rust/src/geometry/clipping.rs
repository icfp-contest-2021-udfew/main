use super::*;

pub enum PointSegment {
    Interior,
    Endpoint(usize), // either 0 or 1
    NotOnSegment,
}

impl PointSegment {
    pub fn in_closure(&self) -> bool {
        match self {
            PointSegment::Interior => true,
            PointSegment::Endpoint(_) => true,
            PointSegment::NotOnSegment => false,
        }
    }
}

pub fn point_on_segment(point: &Point, edge: &Segment) -> PointSegment {
    let v_edge = (edge.1[0] - edge.0[0], edge.1[1] - edge.0[1]);
    let v_point = (point[0] - edge.0[0], point[1] - edge.0[1]);

    if v_point == (0, 0) {
        PointSegment::Endpoint(0)
    } else if v_point == v_edge {
        PointSegment::Endpoint(1)
    } else if v_edge.0.signum() == v_point.0.signum()
        && v_edge.1.signum() == v_point.1.signum()
        && v_edge.0 * v_point.1 == v_edge.1 * v_point.0
        && v_point.0.abs() <= v_edge.0.abs()
        && v_point.1.abs() <= v_edge.1.abs()
    {
        PointSegment::Interior
    } else {
        PointSegment::NotOnSegment
    }
}

pub enum PointPolygon {
    Interior,
    Vertex(usize),       // the vertex it lies on
    EdgeInterior(usize), // the index of the vertex such that the point lies on the edge between this vertex and its successor
    Exterior,
}

impl PointPolygon {
    pub fn in_closure(&self) -> bool {
        match self {
            PointPolygon::Interior => true,
            PointPolygon::Vertex(_) => true,
            PointPolygon::EdgeInterior(_) => true,
            PointPolygon::Exterior => false,
        }
    }
}

// Code adapted from https://wrf.ecse.rpi.edu//Research/Short_Notes/pnpoly.html
#[allow(clippy::ptr_arg)]
pub fn point_in_polygon(point: &Point, polygon: &Polygon) -> PointPolygon {
    let mut c = false;

    for i in 0..polygon.len() {
        let j = if i == 0 { polygon.len() - 1 } else { i - 1 };

        match point_on_segment(point, &(polygon[j], polygon[i])) {
            PointSegment::Interior => return PointPolygon::EdgeInterior(j),
            PointSegment::Endpoint(k) => {
                return PointPolygon::Vertex(if k == 0 { j } else { i })
            }
            PointSegment::NotOnSegment => {}
        }

        if ((polygon[i][1] > point[1]) != (polygon[j][1] > point[1]))
            && ((polygon[j][1] - polygon[i][1]).abs()
                * (point[0] - polygon[i][0])
                < (polygon[j][0] - polygon[i][0])
                    * (point[1] - polygon[i][1])
                    * (polygon[j][1] - polygon[i][1]).signum())
        {
            c = !c;
        }
    }

    if c {
        PointPolygon::Interior
    } else {
        PointPolygon::Exterior
    }
}

#[derive(PartialEq, Eq, Debug)]
pub enum Orientation {
    Colinear,
    Clockwise,
    Counterclockwise,
}

use Orientation::*;

// To find orientation of ordered triplet (p, q, r).
fn orientation(p: &Point, q: &Point, r: &Point) -> Orientation {
    let val = (q[1] - p[1]) * (r[0] - q[0]) - (q[0] - p[0]) * (r[1] - q[1]);

    match val {
        0 => Colinear,
        _ if val > 0 => Counterclockwise,
        _ => Clockwise,
    }
}

// Checks if the interiors of the two segments intersect in exactly one point
// Code adapted from https://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
pub fn segments_intersect((p1, q1): &Segment, (p2, q2): &Segment) -> bool {
    // Given three colinear points p, q, r, the function checks if
    // point q lies on line segment 'pr'
    // fn on_segment(p: &Point, q: &Point, r: &Point) -> bool {
    //     q[0] <= max(p[0], r[0])
    //         && q[0] >= min(p[0], r[0])
    //         && q[1] <= max(p[1], r[1])
    //         && q[1] >= min(p[1], r[1])
    // }

    // Find the four orientations needed for general and
    // special cases
    let o1 = orientation(p1, q1, p2);
    let o2 = orientation(p1, q1, q2);
    let o3 = orientation(p2, q2, p1);
    let o4 = orientation(p2, q2, q1);

    if o1 == Colinear || o2 == Colinear || o3 == Colinear || o4 == Colinear {
        return false;
    }

    o1 != o2 && o3 != o4 // General case

    // || o1 == 0 && on_segment(p1, p2, q1) // p1, q1 and p2 are colinear and p2 lies on segment p1q1
    // || o2 == 0 && on_segment(p1, q2, q1) // p1, q1 and q2 are colinear and q2 lies on segment p1q1
    // || o3 == 0 && on_segment(p2, p1, q2) // p2, q2 and p1 are colinear and p1 lies on segment p2q2
    // || o4 == 0 && on_segment(p2, q1, q2) // p2, q2 and q1 are colinear and q1 lies on segment p2q2
}

fn prev_mod(n: usize, modular: usize) -> usize {
    (n + modular - 1).rem_euclid(modular)
}

#[allow(clippy::ptr_arg)]
pub fn polygon_orientation(polygon: &Polygon) -> Orientation {
    let mut extreme = 0;

    for (i, v) in polygon.iter().enumerate().skip(1) {
        if v[0] < polygon[extreme][0]
            || (v[0] == polygon[extreme][0] && v[1] < polygon[extreme][1])
        {
            extreme = i;
        }
    }

    let prev = prev_mod(extreme, polygon.len());
    let next = (extreme + 1).rem_euclid(polygon.len());

    orientation(&polygon[prev], &polygon[extreme], &polygon[next])
}

#[allow(clippy::ptr_arg)]
fn stays_inside(segment: &Segment, polygon: &Polygon) -> bool {
    if iter_edges(&polygon).any(|edge| segments_intersect(&edge, &segment)) {
        return false;
    }

    for v in polygon.iter() {
        if let PointSegment::Interior = point_on_segment(&v, &segment) {
            return segment_in_polygon(&(segment.0, *v), &polygon)
                && segment_in_polygon(&(segment.1, *v), &polygon);
        }
    }

    true
}

#[allow(clippy::ptr_arg)]
pub fn segment_in_polygon(segment: &Segment, polygon: &Polygon) -> bool {
    let ibe0 = point_in_polygon(&segment.0, polygon);
    let ibe1 = point_in_polygon(&segment.1, polygon);

    if segment.0 == segment.1 && ibe0.in_closure() {
        return true;
    }

    match (ibe0, ibe1) {
        (PointPolygon::Exterior, _) => false,
        (_, PointPolygon::Exterior) => false,
        (PointPolygon::Interior, _) => stays_inside(segment, polygon),
        (PointPolygon::EdgeInterior(i), _) => {
            let next = (i + 1).rem_euclid(polygon.len());
            let or = orientation(&polygon[i], &polygon[next], &segment.1);
            if or == Colinear {
                if point_on_segment(&polygon[i], &segment).in_closure() {
                    segment_in_polygon(&(polygon[i], segment.1), &polygon)
                } else if point_on_segment(&polygon[next], &segment)
                    .in_closure()
                {
                    segment_in_polygon(&(polygon[next], segment.1), &polygon)
                } else {
                    true
                }
            } else {
                polygon_orientation(polygon) == or
                    && stays_inside(segment, polygon)
            }
        }
        (PointPolygon::Vertex(i), _) => {
            let prev = prev_mod(i, polygon.len());
            let next = (i + 1).rem_euclid(polygon.len());

            let or = orientation(&polygon[prev], &polygon[i], &polygon[next]);

            let or0 = orientation(&polygon[prev], &polygon[i], &segment.1);
            let or1 = orientation(&polygon[i], &polygon[next], &segment.1);

            let or_poly = polygon_orientation(polygon);

            let reduce_problem = |&point| {
                if point_on_segment(&point, &segment).in_closure() {
                    stays_inside(&(point, segment.1), &polygon)
                } else {
                    true
                }
            };

            #[allow(clippy::collapsible_else_if)]
            if or == Colinear {
                if or0 == or_poly {
                    stays_inside(segment, polygon)
                } else if or0 == Colinear {
                    if point_on_segment(&polygon[prev], &segment).in_closure() {
                        stays_inside(&(polygon[prev], segment.1), &polygon)
                    } else if point_on_segment(&polygon[next], &segment)
                        .in_closure()
                    {
                        stays_inside(&(polygon[next], segment.1), &polygon)
                    } else {
                        true
                    }
                } else {
                    false
                }
            } else if or == or_poly {
                if or0 == or_poly && or1 == or_poly {
                    stays_inside(segment, polygon)
                } else if or0 == Colinear && or1 == or_poly {
                    reduce_problem(&polygon[prev])
                } else if or0 == or_poly && or1 == Colinear {
                    reduce_problem(&polygon[next])
                } else {
                    false
                }
            } else {
                if or0 == or_poly || or1 == or_poly {
                    stays_inside(segment, polygon)
                } else if or0 == Colinear {
                    reduce_problem(&polygon[prev])
                } else if or1 == Colinear {
                    reduce_problem(&polygon[next])
                } else {
                    false
                }
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_point_on_segment() {
        let test_cases = vec![
            ([0, 0], ([0, 0], [-3, 6])),
            ([1, 1], ([0, 0], [2, 2])),
            ([2, 2], ([0, 1], [4, 3])),
            ([0, 0], ([-2, 5], [4, -10])),
            ([-3, 6], ([0, 0], [-3, 6])),
        ];

        for (v, s) in test_cases {
            assert!(point_on_segment(&v, &s).in_closure());
        }
    }

    #[test]
    fn test_point_in_polygon() {
        let polygon = vec![
            [55, 80],
            [65, 95],
            [95, 95],
            [35, 5],
            [5, 5],
            [35, 50],
            [5, 95],
            [35, 95],
            [45, 80],
        ];
        let in_vertices = vec![
            [21, 28],
            [31, 28],
            [31, 87],
            [29, 41],
            [44, 43],
            [58, 70],
            [38, 79],
            [32, 31],
            [36, 50],
            [39, 40],
            [66, 77],
            [42, 29],
            [46, 49],
            [49, 38],
            [39, 57],
            [69, 66],
            [41, 70],
            [39, 60],
            [42, 25],
            [40, 35],
        ];
        let out_vertices = vec![[-43, 69], [37, 95], [81, 71], [29, 49]];

        for v in in_vertices {
            println!("{:?}", &v);
            assert!(point_in_polygon(&v, &polygon).in_closure());
        }

        for v in out_vertices {
            println!("{:?}", &v);
            assert!(!point_in_polygon(&v, &polygon).in_closure());
        }
    }

    #[test]
    fn test_segments_intersect() {
        assert!(segments_intersect(&([0, 0], [2, 0]), &([0, 1], [1, -1])));
        assert!(segments_intersect(&([0, 0], [2, 2]), &([0, 2], [2, 0])));

        assert!(!segments_intersect(&([0, 0], [1, 1]), &([0, 0], [-3, 6])));
        assert!(!segments_intersect(&([0, 0], [1, 0]), &([0, 1], [0, -7])));
        assert!(!segments_intersect(&([0, 0], [2, 0]), &([-1, 0], [2, 1])));
        assert!(!segments_intersect(&([0, 0], [2, 0]), &([0, 1], [2, 1])));
    }

    #[test]
    fn test_segment_in_polygon() {
        let polygon = vec![
            [55, 80],
            [65, 95],
            [95, 95],
            [35, 5],
            [5, 5],
            [35, 50],
            [5, 95],
            [35, 95],
            [45, 80],
        ];
        let out_segments = vec![
            ([31, 40], [32, 59]),
            ([34, 87], [47, 90]),
            ([35, 95], [61, 89]),
            ([35, 95], [65, 95]),
            ([0, 0], [0, 0]),
        ];
        let in_segments = vec![
            ([35, 95], [15, 80]),
            ([30, 80], [60, 80]),
            ([5, 5], [65, 95]),
            ([30, 32], [30, 32]),
        ];

        for s in in_segments {
            println!("{:?}", &s);
            assert!(segment_in_polygon(&s, &polygon));
        }

        for s in out_segments {
            println!("{:?}", &s);
            assert!(!segment_in_polygon(&s, &polygon));
        }

        let edge_indices = vec![
            [2, 5],
            [5, 4],
            [4, 1],
            [1, 0],
            [0, 8],
            [8, 3],
            [3, 7],
            [7, 11],
            [11, 13],
            [13, 12],
            [12, 18],
            [18, 19],
            [19, 14],
            [14, 15],
            [15, 17],
            [17, 16],
            [16, 10],
            [10, 6],
            [6, 2],
            [8, 12],
            [7, 9],
            [9, 3],
            [8, 9],
            [9, 12],
            [13, 9],
            [9, 11],
            [4, 8],
            [12, 14],
            [5, 10],
            [10, 15],
        ];
        let vertices = vec![
            [21, 28],
            [31, 28],
            [31, 87],
            [29, 41],
            [44, 43],
            [58, 70],
            [38, 79],
            [32, 31],
            [36, 50],
            [39, 40],
            [66, 77],
            [42, 29],
            [46, 49],
            [49, 38],
            [39, 57],
            [69, 66],
            [41, 70],
            [39, 60],
            [42, 25],
            [40, 35],
        ];

        for [i, j] in edge_indices {
            let segment = (vertices[i], vertices[j]);
            println!("{:?}", &segment);
            assert!(segment_in_polygon(&segment, &polygon));
        }
    }
}
