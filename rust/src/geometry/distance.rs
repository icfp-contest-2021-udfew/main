use super::*;
use geo::algorithm::euclidean_distance::EuclideanDistance;

#[allow(clippy::ptr_arg)]
pub fn dist_to_polygon(point: &Point, polygon: &Polygon) -> f64 {
    point_to_geo(&point).euclidean_distance(&polygon_to_geo(&polygon))
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_dist_to_polygon() {
        let polygon = vec![[0, 0], [0, 1], [1, 1], [1, 0]];

        let test_cases = vec![([0, -1], 1.), ([0, 0], 0.)];

        for (p, d) in test_cases {
            assert!((dist_to_polygon(&p, &polygon) - d).abs() <= 0.01);
        }
    }
}
