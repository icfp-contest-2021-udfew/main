use crate::specification::*;
use itertools::Itertools;

pub type Movie = Vec<Pose>;

#[allow(unstable_name_collisions)]
#[allow(clippy::ptr_arg)]
pub fn dump_movie(movie: &Movie) -> String {
    let mut s = String::new();
    s.push('[');
    s.push_str(
        movie
            .iter()
            .map(|pose| pose.dump())
            .intersperse(String::from(","))
            .collect::<String>()
            .as_str(),
    );
    s.push(']');

    s
}
