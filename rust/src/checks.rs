use num::{BigInt, BigRational, Zero};
use std::collections::BTreeMap;
use std::iter::{empty, once};

use super::geometry::clipping::{point_in_polygon, segment_in_polygon};
use super::specification::*;

pub static STRETCH_DENOMINATOR: Scalar = 1_000_000;

pub fn squared_distance(p: &Point, q: &Point) -> Scalar {
    Scalar::pow(p[0] - q[0], 2) + Scalar::pow(p[1] - q[1], 2)
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum PoseError {
    TooManyBonuses,
    EscapingEdge(Edge),
    OverstretchedEdge(Edge, Scalar),
    GlobalOverstretch(BigRational),
}

impl Problem {
    pub fn overstretch(&self, old_d: Scalar, new_d: Scalar) -> Scalar {
        let lhs = Scalar::abs(new_d - old_d) * STRETCH_DENOMINATOR;
        let rhs = self.epsilon * old_d;
        lhs - rhs
    }
}

impl Pose {
    pub fn is_valid_for(&self, problem: &Problem) -> bool {
        self.errors_for(problem).next().is_none()
    }

    pub fn errors_for<'a>(
        &'a self,
        problem: &'a Problem,
    ) -> Box<dyn Iterator<Item = PoseError> + 'a> {
        let mut it: Box<dyn Iterator<Item = PoseError>> = Box::new(
            if self.bonuses.len() > 1 {
                Some(PoseError::TooManyBonuses)
            } else {
                None
            }
            .into_iter(),
        );

        let mut escaping_edges: Box<dyn Iterator<Item = Edge>> =
            Box::new(self.escaping_edges_for(problem));
        if self.wallhack_active() {
            let mut edges = Vec::with_capacity(problem.figure.edges.len());
            // Note that in the presence of escaping edges, a "wallhack" is
            // valid if and only if the following two conditions are satisfied:
            // * There exists an vertex that is outside of the hole and on all
            // escaping edges.
            // * All the other other vertices (on escaping edges) are within
            // the hole.
            //
            // Hence we track the following data for all vertices that occur
            // in an escaping edge:
            // * The number of escaping edges the vertex uccurs in.
            // * Whether the vertex is in the hole.
            let mut data: BTreeMap<usize, (usize, bool)> = BTreeMap::new();
            while let Some(e) = escaping_edges.next() {
                let (i, j) = e;
                edges.push(e);
                data.entry(i)
                    .and_modify(|d| {
                        *d = (d.0 + 1, d.1);
                    })
                    .or_insert((
                        0,
                        point_in_polygon(&self.vertices[i], &problem.hole)
                            .in_closure(),
                    ));
                data.entry(j)
                    .and_modify(|d| {
                        *d = (d.0 + 1, d.1);
                    })
                    .or_insert((
                        0,
                        point_in_polygon(&self.vertices[j], &problem.hole)
                            .in_closure(),
                    ));
            }
            let mut outsiders =
                data.iter().filter_map(
                    |(v, (_, inside))| {
                        if *inside {
                            None
                        } else {
                            Some(*v)
                        }
                    },
                );
            escaping_edges = match outsiders.next() {
                Some(outsider) => {
                    if data[&outsider].0 == edges.len()
                        && outsiders.next().is_none()
                    {
                        Box::new(empty())
                    } else {
                        Box::new(edges.into_iter())
                    }
                }
                None => Box::new(edges.into_iter()),
            }
        }
        it = Box::new(it.chain(escaping_edges.map(PoseError::EscapingEdge)));

        if self.globalist_active() {
            Box::new(
                it.chain(
                    self.global_overstretch_for(problem)
                        .into_iter()
                        .map(PoseError::GlobalOverstretch),
                ),
            )
        } else {
            Box::new(
                it.chain(
                    self.overstretched_edges_for(problem)
                        .map(|(e, x)| PoseError::OverstretchedEdge(e, x))
                        .skip(if self.superflex_active() { 1 } else { 0 }),
                ),
            )
        }
    }

    pub fn annotated_edges_for<'a>(
        &'a self,
        problem: &'a Problem,
    ) -> Box<dyn Iterator<Item = (Edge, bool)> + 'a> {
        if let Some(edge) = self.broken_leg() {
            let (i, j) = edge;
            let new_index = problem.figure.vertices.len();
            Box::new(
                problem
                    .figure
                    .edges
                    .iter()
                    .copied()
                    .filter_map(
                        move |e| {
                            if e == edge {
                                None
                            } else {
                                Some((e, false))
                            }
                        },
                    )
                    .chain(once(((i, new_index), true)))
                    .chain(once(((new_index, j), true))),
            )
        } else {
            Box::new(problem.figure.edges.iter().map(|e| (*e, false)))
        }
    }

    pub fn edges_for<'a>(
        &'a self,
        problem: &'a Problem,
    ) -> impl Iterator<Item = Edge> + 'a {
        self.annotated_edges_for(problem).map(|(e, _)| e)
    }

    // NOT wallhack-aware.
    pub fn escaping_edges_for<'a>(
        &'a self,
        problem: &'a Problem,
    ) -> impl Iterator<Item = Edge> + 'a {
        self.edges_for(problem).filter(move |(i, j)| {
            let segment = (self.vertices[*i], self.vertices[*j]);
            !segment_in_polygon(&segment, &problem.hole)
        })
    }

    pub fn edges_with_distances_for<'a>(
        &'a self,
        problem: &'a Problem,
    ) -> impl Iterator<Item = (Edge, Scalar, Scalar)> + 'a {
        self.annotated_edges_for(problem)
            .map(move |(edge, broken)| {
                let (i, j) = edge;
                let old_d = if broken {
                    let (k, l) = self.broken_leg().unwrap();
                    squared_distance(
                        &problem.figure.vertices[k],
                        &problem.figure.vertices[l],
                    )
                } else {
                    squared_distance(
                        &problem.figure.vertices[i],
                        &problem.figure.vertices[j],
                    )
                };
                let new_d = if broken { 4 } else { 1 }
                    * squared_distance(&self.vertices[i], &self.vertices[j]);
                (edge, old_d, new_d)
            })
    }

    // NOT superflex-aware.
    pub fn overstretched_edges_for<'a>(
        &'a self,
        problem: &'a Problem,
    ) -> impl Iterator<Item = (Edge, Scalar)> + 'a {
        self.edges_with_distances_for(problem).filter_map(
            move |(edge, old_d, new_d)| {
                let overstretch = problem.overstretch(old_d, new_d);
                if overstretch > 0 {
                    Some((edge, overstretch / old_d))
                } else {
                    None
                }
            },
        )
    }

    pub fn global_overstretch_for(
        &self,
        problem: &Problem,
    ) -> Option<BigRational> {
        let mut lhs = BigRational::zero();

        for (i, j) in self.edges_for(problem) {
            let old_d = squared_distance(
                &problem.figure.vertices[i],
                &problem.figure.vertices[j],
            );
            let new_d = squared_distance(&self.vertices[i], &self.vertices[j]);
            lhs += BigRational::new_raw(
                BigInt::from(Scalar::abs(new_d - old_d)),
                BigInt::from(old_d),
            )
            .reduced();
        }

        let rhs = BigRational::new_raw(
            BigInt::from(
                problem.figure.edges.len() as Scalar * problem.epsilon,
            ),
            BigInt::from(STRETCH_DENOMINATOR),
        )
        .reduced();

        if lhs < rhs {
            None
        } else {
            lhs -= rhs;
            Some(lhs)
        }
    }

    pub fn dislikes_for(&self, problem: &Problem) -> Scalar {
        problem
            .hole
            .iter()
            .map(|p| {
                self.vertices
                    .iter()
                    .map(|q| squared_distance(p, q))
                    .min()
                    .unwrap()
            })
            .sum()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn trivial_pose() {
        let problem = Problem {
            hole: vec![[0, 0], [10, 0], [10, 10], [0, 10]],
            figure: Figure {
                vertices: vec![[0, 0], [5, 10], [10, 0]],
                edges: vec![(0, 1), (1, 2), (2, 0)],
            },
            epsilon: 100_000,
            bonuses: vec![],
        };
        let pose = Pose {
            vertices: problem.figure.vertices.clone(),
            bonuses: vec![],
        };
        assert!(pose.is_valid_for(&problem));
    }

    #[test]
    fn escaping_edge() {
        let problem = Problem {
            hole: vec![[0, 0], [10, 0], [10, 10], [0, 10]],
            figure: Figure {
                vertices: vec![[0, 0], [5, 11], [10, 0]],
                edges: vec![(0, 1), (1, 2), (2, 0)],
            },
            epsilon: 100_000,
            bonuses: vec![],
        };
        let pose = Pose {
            vertices: problem.figure.vertices.clone(),
            bonuses: Vec::new(),
        };
        assert_eq!(
            pose.escaping_edges_for(&problem).collect::<Vec<_>>(),
            vec![(0, 1), (1, 2)],
        );
    }
}
