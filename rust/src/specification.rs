pub type Scalar = i64;
pub type Point = [Scalar; 2];
pub type Edge = (usize, usize);
pub type Polygon = Vec<Point>;

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Figure {
    pub vertices: Vec<Point>,
    pub edges: Vec<Edge>,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum BonusType {
    Globalist,
    BreakALeg,
    Wallhack,
    Superflex,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct ProblemBonus {
    pub position: Point,
    pub bonus: BonusType,
    pub problem: usize,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Problem {
    pub hole: Polygon,
    pub figure: Figure,
    pub epsilon: Scalar,
    pub bonuses: Vec<ProblemBonus>,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum BonusUsage {
    Globalist,
    BreakALeg(Edge),
    Wallhack,
    Superflex,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct PoseBonus {
    pub bonus: BonusUsage,
    pub problem: usize,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Pose {
    pub vertices: Vec<Point>,
    pub bonuses: Vec<PoseBonus>,
}

impl Pose {
    pub fn broken_leg(&self) -> Option<Edge> {
        let pose_bonus = self.bonuses.first()?;
        match pose_bonus.bonus {
            BonusUsage::BreakALeg((k, l)) => Some((k, l)),
            _ => None,
        }
    }
    pub fn globalist_active(&self) -> bool {
        if let Some(pose_bonus) = self.bonuses.first() {
            pose_bonus.bonus == BonusUsage::Globalist
        } else {
            false
        }
    }
    pub fn wallhack_active(&self) -> bool {
        if let Some(pose_bonus) = self.bonuses.first() {
            pose_bonus.bonus == BonusUsage::Wallhack
        } else {
            false
        }
    }
    pub fn superflex_active(&self) -> bool {
        if let Some(pose_bonus) = self.bonuses.first() {
            pose_bonus.bonus == BonusUsage::Superflex
        } else {
            false
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Context {
    pub problem: Problem,
    pub pose: Pose,
}
