use num::BigInt;

use super::*;
use crate::checks::STRETCH_DENOMINATOR;

#[derive(Clone, Copy, Debug)]
pub struct Overstretch();

// #[derive(Clone, Debug, Default)]
// pub struct OverstretchScore(Vec<f64>);

// impl PartialEq for OverstretchScore {
//     fn eq(&self, other: &Self) -> bool {
//         self.0.iter().sum::<f64>() == other.0.iter().sum::<f64>()
//     }
// }

// impl PartialOrd for OverstretchScore {
//     fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
//         self.0
//             .iter()
//             .sum::<f64>()
//             .partial_cmp(&other.0.iter().sum::<f64>())
//     }
// }

impl Scorer for Overstretch {
    type Score = u64;
    fn score(&mut self, problem: &Problem, pose: &Pose) -> Self::Score {
        if pose.globalist_active() {
            match pose.global_overstretch_for(problem) {
                None => 0,
                Some(mut overstretch) => {
                    overstretch *= BigInt::from(STRETCH_DENOMINATOR as u64);
                    let digits =
                        overstretch.ceil().to_integer().to_u64_digits().1;
                    if digits.len() > 1 {
                        u64::MAX
                    } else {
                        digits[0] as u64
                    }
                }
            }
        } else if pose.superflex_active() {
            let mut score = 0;
            let mut max = None;
            for (_, x) in pose.overstretched_edges_for(problem) {
                score += x as u64;
                if max.is_none() || x > max.unwrap() {
                    max = Some(x);
                }
            }
            if let Some(m) = max {
                score -= m as u64;
            }
            score
        } else {
            pose.overstretched_edges_for(problem)
                .map(|(_, x)| x as u64)
                .sum()
        }
    }

    fn update(
        &mut self,
        score: &mut Self::Score,
        _: &Move,
        problem: &Problem,
        new: &Pose,
    ) {
        *score = self.score(problem, new);
    }
}
