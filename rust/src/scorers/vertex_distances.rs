use std::collections::HashMap;

use super::*;
use crate::geometry::{clipping::point_in_polygon, distance::dist_to_polygon};

#[derive(Clone, Debug)]
pub struct VertexDistances(HashMap<Point, f64>);

impl VertexDistances {
    pub fn new() -> Self {
        VertexDistances(HashMap::new())
    }

    fn point_score(&mut self, p: &Point, problem: &Problem) -> f64 {
        if point_in_polygon(p, &problem.hole).in_closure() {
            0.
        } else {
            dist_to_polygon(p, &problem.hole)
        }
    }

    fn point_score_mem(&mut self, point: &Point, problem: &Problem) -> f64 {
        if let Some(s) = self.0.get(point) {
            *s
        } else {
            let s = self.point_score(&point, &problem);
            self.0.insert(*point, s);
            s
        }
    }
}

#[derive(Clone, Debug, Default)]
pub struct VertexDistancesScore(Vec<f64>);

impl PartialEq for VertexDistancesScore {
    fn eq(&self, other: &Self) -> bool {
        self.0.iter().sum::<f64>() == other.0.iter().sum::<f64>()
    }
}

impl PartialOrd for VertexDistancesScore {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.0
            .iter()
            .sum::<f64>()
            .partial_cmp(&other.0.iter().sum::<f64>())
    }
}

impl Scorer for VertexDistances {
    type Score = VertexDistancesScore;
    fn score(&mut self, problem: &Problem, pose: &Pose) -> Self::Score {
        VertexDistancesScore(
            pose.vertices
                .iter()
                .map(|p| self.point_score_mem(p, problem))
                .collect(),
        )
    }

    fn update(
        &mut self,
        score: &mut Self::Score,
        m: &Move,
        problem: &Problem,
        new: &Pose,
    ) {
        match m {
            Move::Vertex(i, _) => {
                score.0[*i] = self.point_score_mem(&new.vertices[*i], problem);
            }
            Move::Pose(_) => *score = self.score(problem, new),
            Move::Pointwise(_) => *score = self.score(problem, new),
        }
    }
}
