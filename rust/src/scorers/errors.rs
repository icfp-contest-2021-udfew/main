use super::*;
use crate::specification::*;

#[derive(Clone, Copy, Debug)]
pub struct Errors();

impl Scorer for Errors {
    type Score = usize;
    fn score(&mut self, problem: &Problem, pose: &Pose) -> Self::Score {
        pose.errors_for(problem).count()
    }

    fn update(
        &mut self,
        score: &mut Self::Score,
        _: &Move,
        problem: &Problem,
        new: &Pose,
    ) {
        *score = self.score(problem, new);
    }
}
