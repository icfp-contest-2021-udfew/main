pub mod area_cut_out;
pub use area_cut_out::AreaCutOut;
pub mod dislikes;
pub use dislikes::Dislikes;
pub mod errors;
pub use errors::Errors;
pub mod overstretch;
pub use overstretch::Overstretch;
pub mod vertex_distances;
pub use vertex_distances::VertexDistances;

use std::cmp::Reverse;
use std::fmt::Debug;
// use std::ops::{Add, Mul};

use crate::move_factories::Move;
use crate::specification::*;

pub type PunishOutside = LeftBiased<VertexDistances, AreaCutOut>;

impl PunishOutside {
    pub fn new(pose: &Pose, problem: &Problem) -> Self {
        LeftBiased(VertexDistances::new(), AreaCutOut::new(pose, problem))
    }
}

pub trait Scorer {
    type Score: Clone + Debug + PartialOrd;
    fn score(&mut self, _: &Problem, _: &Pose) -> Self::Score;
    fn update(
        &mut self,
        score: &mut Self::Score,
        m: &Move,
        problem: &Problem,
        new: &Pose,
    );
}

pub struct Reversed<S>(pub S);

impl<S: Scorer> Scorer for Reversed<S> {
    type Score = Reverse<S::Score>;
    fn score(&mut self, problem: &Problem, pose: &Pose) -> Self::Score {
        Reverse(self.0.score(problem, pose))
    }

    fn update(
        &mut self,
        score: &mut Self::Score,
        m: &Move,
        problem: &Problem,
        new: &Pose,
    ) {
        self.0.update(&mut score.0, m, problem, new);
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct LeftBiased<S, T>(pub S, pub T);

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct LeftBiasedScore<N, M> {
    larger: N,
    smaller: Option<M>,
}

impl<N: Default, M: Default> Default for LeftBiasedScore<N, M> {
    fn default() -> Self {
        LeftBiasedScore {
            smaller: Some(M::default()),
            larger: N::default(),
        }
    }
}

impl<
        N: Clone + Debug + PartialOrd + PartialEq + Default,
        M: Clone + Debug + PartialOrd + PartialEq + Default,
        S: Scorer<Score = N>,
        T: Scorer<Score = M>,
    > Scorer for LeftBiased<S, T>
{
    type Score = LeftBiasedScore<N, M>;
    fn score(&mut self, problem: &Problem, pose: &Pose) -> Self::Score {
        let first = self.0.score(problem, pose);
        LeftBiasedScore {
            smaller: if first != N::default() {
                None
            } else {
                Some(self.1.score(problem, pose))
            },
            larger: first,
        }
    }

    fn update(
        &mut self,
        score: &mut Self::Score,
        m: &Move,
        problem: &Problem,
        new: &Pose,
    ) {
        self.0.update(&mut score.larger, m, problem, new);
        if score.larger == N::default() {
            if let Some(s) = &mut score.smaller {
                self.1.update(s, m, problem, new);
            } else {
                score.smaller = Some(self.1.score(problem, new));
            }
        } else {
            score.smaller = None;
        }
    }
}

impl<S: Scorer, T: Scorer> Scorer for (S, T) {
    type Score = (S::Score, T::Score);
    fn score(&mut self, problem: &Problem, pose: &Pose) -> Self::Score {
        (self.0.score(problem, pose), self.1.score(problem, pose))
    }

    fn update(
        &mut self,
        score: &mut Self::Score,
        m: &Move,
        problem: &Problem,
        new: &Pose,
    ) {
        self.0.update(&mut score.0, m, problem, new);
        self.1.update(&mut score.1, m, problem, new);
    }
}

impl<S: Scorer, T: Scorer, U: Scorer> Scorer for (S, T, U) {
    type Score = (S::Score, T::Score, U::Score);
    fn score(&mut self, problem: &Problem, pose: &Pose) -> Self::Score {
        (
            self.0.score(problem, pose),
            self.1.score(problem, pose),
            self.2.score(problem, pose),
        )
    }

    fn update(
        &mut self,
        score: &mut Self::Score,
        m: &Move,
        problem: &Problem,
        new: &Pose,
    ) {
        self.0.update(&mut score.0, m, problem, new);
        self.1.update(&mut score.1, m, problem, new);
        self.2.update(&mut score.2, m, problem, new);
    }
}

impl<S: Scorer, T: Scorer, U: Scorer, V: Scorer> Scorer for (S, T, U, V) {
    type Score = (S::Score, T::Score, U::Score, V::Score);
    fn score(&mut self, problem: &Problem, pose: &Pose) -> Self::Score {
        (
            self.0.score(problem, pose),
            self.1.score(problem, pose),
            self.2.score(problem, pose),
            self.3.score(problem, pose),
        )
    }

    fn update(
        &mut self,
        score: &mut Self::Score,
        m: &Move,
        problem: &Problem,
        new: &Pose,
    ) {
        self.0.update(&mut score.0, m, problem, new);
        self.1.update(&mut score.1, m, problem, new);
        self.2.update(&mut score.2, m, problem, new);
        self.3.update(&mut score.3, m, problem, new);
    }
}

// pub struct LinearCombination<S, T, N>(pub N, pub S, pub N, pub T);

// impl<
//         N: Copy + Debug + PartialOrd + Add<Output = N> + Mul<Output = N>,
//         S: Scorer<Score = N>,
//         T: Scorer<Score = N>,
//     > Scorer for LinearCombination<S, T, N>
// {
//     type Score = N;
//     fn score(&mut self, problem: &Problem, pose: &Pose) -> Self::Score {
//         self.0 * self.1.score(problem, pose)
//             + self.2 * self.3.score(problem, pose)
//     }
// }

// #[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
// pub enum AdjoinMaximum<T> {
//     Inner(T),
//     Maximum,
// }

// pub struct WithoutEscaping<S>(pub S);

// impl<S: Scorer> Scorer for WithoutEscaping<S> {
//     type Score = AdjoinMaximum<S::Score>;
//     fn score(&mut self, problem: &Problem, pose: &Pose) -> Self::Score {
//         if pose.escaping_edges_for(problem).next().is_some() {
//             AdjoinMaximum::Maximum
//         } else {
//             AdjoinMaximum::Inner(self.0.score(problem, pose))
//         }
//     }
// }

// pub struct WithoutOverstretched<S>(pub S);

// impl<S: Scorer> Scorer for WithoutOverstretched<S> {
//     type Score = AdjoinMaximum<S::Score>;
//     fn score(&mut self, problem: &Problem, pose: &Pose) -> Self::Score {
//         if pose.overstretched_edges_for(problem).next().is_some() {
//             AdjoinMaximum::Maximum
//         } else {
//             AdjoinMaximum::Inner(self.0.score(problem, pose))
//         }
//     }
// }

// pub type AlwaysValid<S> = WithoutEscaping<WithoutOverstretched<S>>;

// pub fn always_valid<S>(s: S) -> AlwaysValid<S> {
//     WithoutEscaping(WithoutOverstretched(s))
// }
