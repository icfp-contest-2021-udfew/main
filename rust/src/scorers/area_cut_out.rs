use std::collections::HashMap;

use super::*;
use crate::geometry::area::area_cut_out;
use crate::geometry::clipping::{point_in_polygon, segment_in_polygon};
use crate::graph::EdgeLenGraph;
use crate::specification::*;

#[derive(Clone, Debug)]
pub struct AreaCutOut(HashMap<Point, bool>, EdgeLenGraph);

impl AreaCutOut {
    pub fn new(pose: &Pose, problem: &Problem) -> Self {
        AreaCutOut(HashMap::new(), problem.graph(pose))
    }

    fn edge_score(
        &mut self,
        edge: &Edge,
        problem: &Problem,
        pose: &Pose,
    ) -> f64 {
        let (i, j) = edge;
        let segment = (pose.vertices[*i], pose.vertices[*j]);
        if self.point_in_polygon_mem(&segment.0, &problem.hole)
            && self.point_in_polygon_mem(&segment.1, &problem.hole)
            && !segment_in_polygon(&segment, &problem.hole)
        {
            area_cut_out(&segment, &problem.hole)
        } else {
            0.
        }
    }

    fn point_in_polygon_mem(
        &mut self,
        point: &Point,
        polygon: &Polygon,
    ) -> bool {
        if let Some(b) = self.0.get(point) {
            *b
        } else {
            let b = point_in_polygon(&point, &polygon).in_closure();
            self.0.insert(*point, b);
            b
        }
    }
}

#[derive(Clone, Debug, Default)]
pub struct AreaCutOutScore(Vec<f64>);

impl PartialEq for AreaCutOutScore {
    fn eq(&self, other: &Self) -> bool {
        self.0.iter().sum::<f64>() == other.0.iter().sum::<f64>()
    }
}

impl PartialOrd for AreaCutOutScore {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.0
            .iter()
            .sum::<f64>()
            .partial_cmp(&other.0.iter().sum::<f64>())
    }
}

impl Scorer for AreaCutOut {
    type Score = AreaCutOutScore;
    fn score(&mut self, problem: &Problem, pose: &Pose) -> Self::Score {
        AreaCutOutScore(
            pose.edges_for(problem)
                .map(|edge| self.edge_score(&edge, problem, pose))
                .collect(),
        )
    }

    fn update(
        &mut self,
        score: &mut Self::Score,
        m: &Move,
        problem: &Problem,
        new: &Pose,
    ) {
        match m {
            Move::Vertex(vertex_i, _) => {
                for (edge_i, edge) in new.edges_for(problem).enumerate() {
                    if edge.0 == *vertex_i || edge.1 == *vertex_i {
                        score.0[edge_i] = self.edge_score(&edge, problem, new);
                    }
                }
            }
            Move::Pose(_) => *score = self.score(problem, new),
            Move::Pointwise(_) => *score = self.score(problem, new),
        }
    }
}
