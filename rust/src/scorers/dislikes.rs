use super::*;
use crate::specification::*;

#[derive(Clone, Copy, Debug)]
pub struct Dislikes();

// #[derive(Clone, Debug, Default)]
// pub struct DislikesScore(Vec<f64>);

// impl PartialEq for DislikesScore {
//     fn eq(&self, other: &Self) -> bool {
//         self.0.iter().sum::<f64>() == other.0.iter().sum::<f64>()
//     }
// }

// impl PartialOrd for DislikesScore {
//     fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
//         self.0
//             .iter()
//             .sum::<f64>()
//             .partial_cmp(&other.0.iter().sum::<f64>())
//     }
// }

impl Scorer for Dislikes {
    type Score = Scalar;
    fn score(&mut self, problem: &Problem, pose: &Pose) -> Self::Score {
        pose.dislikes_for(problem)
    }

    fn update(
        &mut self,
        score: &mut Self::Score,
        _: &Move,
        problem: &Problem,
        new: &Pose,
    ) {
        *score = self.score(problem, new);
    }
}
