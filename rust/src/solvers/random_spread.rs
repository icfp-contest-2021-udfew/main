use rand::{thread_rng, Rng};

use super::*;
use crate::geometry::floating::*;
use crate::optimizers::{Cinema, Optimizer};
use crate::specification::*;

pub struct RandomSpread<O>(pub O, pub Floating);

impl<O: Optimizer> Solver for RandomSpread<O> {
    fn solve(
        &mut self,
        problem: &Problem,
        max_iterations: Option<usize>,
        cinema: &mut Cinema,
    ) -> Pose {
        let mut rng = thread_rng();

        let mut x_min = Scalar::MAX;
        let mut x_max = Scalar::MIN;
        let mut y_min = Scalar::MAX;
        let mut y_max = Scalar::MIN;
        for v in problem.hole.iter() {
            if v[0] < x_min {
                x_min = v[0];
            }
            if v[0] > x_max {
                x_max = v[0];
            }
            if v[1] < y_min {
                y_min = v[1];
            }
            if v[1] > y_max {
                y_max = v[1];
            }
        }
        let x_diff = x_max - x_min;
        let y_diff = y_max - y_min;
        let middle = [x_diff / 2, y_diff / 2];

        let mut vertices = vec![middle; problem.figure.vertices.len()];
        for (i, j) in problem.figure.edges.iter() {
            let a = rng.gen_range(0.0..2.0 * PI);
            let l = distance(
                &problem.figure.vertices[*i],
                &problem.figure.vertices[*j],
            ) * self.1;
            let v = polar_to_cartesian(a, l);
            vertices[*j][0] += Floating::round(v[0]) as Scalar;
            vertices[*j][1] += Floating::round(v[1]) as Scalar;
        }

        let mut pose = Pose {
            vertices,
            bonuses: Vec::new(),
        };
        self.0.optimize(problem, &mut pose, max_iterations, cinema);

        pose
    }
}
