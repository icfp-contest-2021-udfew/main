use super::*;
use crate::optimizers::{Cinema, Optimizer};
use crate::specification::*;

pub struct StartRotated<O>(pub Rotation, pub O);

pub enum Rotation {
    Clockwise,
    Counterclockwise,
    Flipped,
}

impl Rotation {
    fn rotate(&self, vector: Point) -> Point {
        match self {
            Rotation::Clockwise => [-vector[1], vector[0]],
            Rotation::Counterclockwise => [vector[1], -vector[0]],
            Rotation::Flipped => [-vector[0], -vector[1]],
        }
    }
}

impl<O: Optimizer> Solver for StartRotated<O> {
    fn solve(
        &mut self,
        problem: &Problem,
        max_iterations: Option<usize>,
        cinema: &mut Cinema,
    ) -> Pose {
        let xs: Vec<_> =
            problem.figure.vertices.iter().map(|[x, _]| x).collect();
        let ys: Vec<_> = problem.hole.iter().map(|[_, y]| y).collect();
        let x = (*xs.iter().max().unwrap() + *xs.iter().min().unwrap()) / 2;
        let y = (*ys.iter().max().unwrap() + *ys.iter().min().unwrap()) / 2;

        let mut pose = Pose {
            vertices: problem
                .figure
                .vertices
                .clone()
                .into_iter()
                .map(|[px, py]| {
                    let [qx, qy] = self.0.rotate([px - x, py - y]);
                    [qx + x, qy + y]
                })
                .collect(),
            bonuses: Vec::new(),
        };

        self.1.optimize(problem, &mut pose, max_iterations, cinema);

        pose
    }
}
