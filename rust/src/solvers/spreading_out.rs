pub struct SpreadingOut<O>(pub O);

use super::*;
use crate::checks::squared_distance;
use crate::geometry::clipping::segment_in_polygon;
use crate::optimizers::{Cinema, Optimizer};
use crate::specification::*;

impl<O: Optimizer> Solver for SpreadingOut<O> {
    fn solve(
        &mut self,
        problem: &Problem,
        max_iterations: Option<usize>,
        cinema: &mut Cinema,
    ) -> Pose {
        let mut diagonal = ([0; 2], [0; 2]);
        for p in problem.hole.iter() {
            for q in problem.hole.iter() {
                if squared_distance(p, q)
                    > squared_distance(&diagonal.0, &diagonal.1)
                    && segment_in_polygon(&(*p, *q), &problem.hole)
                {
                    diagonal = (*p, *q);
                }
            }
        }

        let vector =
            [diagonal.1[0] - diagonal.0[0], diagonal.1[1] - diagonal.0[1]];
        let mid_point =
            [diagonal.0[0] + vector[0] / 2, diagonal.0[1] + vector[1] / 2];
        let mut pose = Pose {
            vertices: vec![mid_point; problem.figure.vertices.len()],
            bonuses: Vec::new(),
        };

        // let half = max_iterations.map(|m| m / 2);
        self.0.optimize(problem, &mut pose, max_iterations, cinema);

        pose
    }
}
