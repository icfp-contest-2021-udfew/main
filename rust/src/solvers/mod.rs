pub mod spreading_out;
pub use spreading_out::SpreadingOut;
pub mod random_spread;
pub use random_spread::RandomSpread;
pub mod start_rotated;
pub use start_rotated::{Rotation, StartRotated};

use crate::optimizers::{Cinema, Optimizer};
use crate::specification::*;

pub trait Solver {
    fn solve(&mut self, _: &Problem, _: Option<usize>, _: &mut Cinema) -> Pose;
}

impl<T: Optimizer> Solver for T {
    fn solve(
        &mut self,
        problem: &Problem,
        max_iterations: Option<usize>,
        cinema: &mut Cinema,
    ) -> Pose {
        let mut pose = Pose {
            vertices: problem.figure.vertices.clone(),
            bonuses: Vec::new(),
        };
        self.optimize(problem, &mut pose, max_iterations, cinema);
        pose
    }
}
