use json::{array, object, JsonValue};
use std::convert::{From, TryFrom};
use std::fs::File;
use std::io::Read;
use std::path::Path;

use super::specification::*;

fn point_from_json(json_array: &JsonValue) -> Point {
    [
        json_array[0].as_i64().unwrap(),
        json_array[1].as_i64().unwrap(),
    ]
}

fn points_from_json(json_array: &JsonValue) -> Vec<Point> {
    json_array.members().map(point_from_json).collect()
}

fn edge_from_json(json_array: &JsonValue) -> Edge {
    (
        json_array[0].as_usize().unwrap(),
        json_array[1].as_usize().unwrap(),
    )
}

fn edges_from_json(json_array: &JsonValue) -> Vec<Edge> {
    json_array.members().map(edge_from_json).collect()
}

impl TryFrom<&JsonValue> for BonusType {
    type Error = String;
    fn try_from(json: &JsonValue) -> Result<Self, Self::Error> {
        match json.clone().take_string() {
            Some(s) => match s.as_str() {
                "GLOBALIST" => Ok(BonusType::Globalist),
                "BREAK_A_LEG" => Ok(BonusType::BreakALeg),
                "WALLHACK" => Ok(BonusType::Wallhack),
                "SUPERFLEX" => Ok(BonusType::Superflex),
                _ => Err(s),
            },
            None => panic!("bonus value is not a string"),
        }
    }
}

impl<'a> TryFrom<&'a JsonValue> for ProblemBonus {
    type Error = <BonusType as TryFrom<&'a JsonValue>>::Error;
    fn try_from(json: &JsonValue) -> Result<Self, Self::Error> {
        Ok(ProblemBonus {
            position: point_from_json(&json["position"]),
            bonus: BonusType::try_from(&json["bonus"])?,
            problem: json["problem"].as_usize().unwrap(),
        })
    }
}

impl From<&JsonValue> for Problem {
    fn from(json: &JsonValue) -> Self {
        Problem {
            hole: points_from_json(&json["hole"]),
            figure: Figure {
                edges: edges_from_json(&json["figure"]["edges"]),
                vertices: points_from_json(&json["figure"]["vertices"]),
            },
            epsilon: json["epsilon"].as_i64().unwrap(),
            bonuses: json["bonuses"]
                .members()
                .filter_map(|j| match ProblemBonus::try_from(j) {
                    Ok(b) => Some(b),
                    Err(s) => {
                        eprintln!("bonus type \"{}\" not recognized", s);
                        None
                    }
                })
                .collect(),
        }
    }
}

impl<'a> TryFrom<&'a JsonValue> for PoseBonus {
    type Error = <BonusType as TryFrom<&'a JsonValue>>::Error;
    fn try_from(json: &JsonValue) -> Result<Self, Self::Error> {
        let problem = json["problem"].as_usize().unwrap();
        let bonus_type = BonusType::try_from(&json["bonus"])?;
        match bonus_type {
            BonusType::Globalist => Ok(PoseBonus {
                bonus: BonusUsage::Globalist,
                problem,
            }),
            BonusType::BreakALeg => Ok(PoseBonus {
                bonus: BonusUsage::BreakALeg(edge_from_json(&json["edge"])),
                problem,
            }),
            BonusType::Wallhack => Ok(PoseBonus {
                bonus: BonusUsage::Wallhack,
                problem,
            }),
            BonusType::Superflex => Ok(PoseBonus {
                bonus: BonusUsage::Superflex,
                problem,
            }),
        }
    }
}

impl From<&JsonValue> for Pose {
    fn from(json: &JsonValue) -> Self {
        Pose {
            vertices: points_from_json(&json["vertices"]),
            bonuses: json["bonuses"]
                .members()
                .filter_map(|j| match PoseBonus::try_from(j) {
                    Ok(b) => Some(b),
                    Err(s) => {
                        eprintln!("bonus type \"{}\" not recognized", s);
                        None
                    }
                })
                .collect(),
        }
    }
}

impl From<&Pose> for JsonValue {
    fn from(pose: &Pose) -> Self {
        object! {
            vertices: pose.vertices.iter()
                .map(|point| array![point[0], point[1]])
                .collect::<Vec<json::JsonValue>>(),
            bonuses: pose.bonuses.iter()
                .map(|PoseBonus{bonus, problem}| {
                    match bonus {
                        BonusUsage::Globalist => {
                            object! {
                                bonus: "GLOBALIST",
                                problem: *problem,
                            }
                        },
                        BonusUsage::BreakALeg(edge) => {
                            object! {
                                bonus: "BREAK_A_LEG",
                                edge: array![edge.0, edge.1],
                                problem: *problem,
                            }
                        },
                        BonusUsage::Wallhack => {
                            object! {
                                bonus: "WALLHACK",
                                problem: *problem,
                            }
                        },
                        BonusUsage::Superflex => {
                            object! {
                                bonus: "SUPERFLEX",
                                problem: *problem,
                            }
                        },
                     }
                })
                .collect::<Vec<json::JsonValue>>(),
        }
    }
}

impl Pose {
    pub fn dump(&self) -> String {
        JsonValue::from(self).dump()
    }
}

impl From<&JsonValue> for Context {
    fn from(json: &JsonValue) -> Self {
        Context {
            problem: Problem::from(&json["problem"]),
            pose: Pose::from(&json["pose"]),
        }
    }
}

macro_rules! parsing_boilerplate_for {
    ($t:ty) => {
        impl $t {
            pub fn parse_file<P: AsRef<Path>>(path: P) -> Self {
                <$t>::parse_read(&mut File::open(path).unwrap())
            }

            pub fn parse_read<R: Read>(r: &mut R) -> Self {
                let mut contents = String::new();
                r.read_to_string(&mut contents).unwrap();

                <$t>::parse(&contents)
            }

            pub fn parse(problem_str: &str) -> Self {
                let json = &json::parse(problem_str).unwrap();
                <$t>::from(json)
            }
        }
    };
}

parsing_boilerplate_for!(Problem);
parsing_boilerplate_for!(Pose);
parsing_boilerplate_for!(Context);
