use std::iter::empty;

use super::*;
use crate::specification::*;

pub struct PoseTranslations();

impl MoveFactory for PoseTranslations {
    fn get_moves(
        &mut self,
        _: &Problem,
        _: &Pose,
        fixed: &HashSet<usize>,
    ) -> Box<dyn Iterator<Item = Move>> {
        if fixed.is_empty() {
            Box::new(DIRECTIONS.iter().map(|dir| Move::Pose(*dir)))
        } else {
            Box::new(empty())
        }
    }
}
