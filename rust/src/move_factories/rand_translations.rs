use rand::{thread_rng, Rng};

use super::*;
use crate::specification::*;

pub struct RandTranslations {
    num: usize,
    diam_x: Scalar,
    diam_y: Scalar,
}

impl RandTranslations {
    pub fn new(num: usize, problem: &Problem) -> Self {
        let xs: Vec<_> = problem.hole.iter().map(|[x, _]| x).collect();
        let ys: Vec<_> = problem.hole.iter().map(|[_, y]| y).collect();
        RandTranslations {
            num,
            diam_x: *xs.iter().max().unwrap() - *xs.iter().min().unwrap(),
            diam_y: *ys.iter().max().unwrap() - *ys.iter().min().unwrap(),
        }
    }
}

pub struct RandTranslationIter {
    vertex: usize,
    diam_x: Scalar,
    diam_y: Scalar,
}

impl Iterator for RandTranslationIter {
    type Item = Move;

    fn next(&mut self) -> Option<Self::Item> {
        let mut x: Scalar = 0;
        let mut y: Scalar = 0;
        while x.abs() <= 1 && y.abs() <= 1 {
            x = thread_rng().gen_range((-self.diam_x / 2)..self.diam_x / 2);
            y = thread_rng().gen_range((-self.diam_y / 2)..self.diam_y / 2);
        }
        Some(Move::Vertex(self.vertex, [x, y]))
    }
}

impl MoveFactory for RandTranslations {
    fn get_moves<'a>(
        &'a mut self,
        _: &Problem,
        pose: &Pose,
        fixed: &'a HashSet<usize>,
    ) -> Box<dyn Iterator<Item = Move> + 'a> {
        let n = self.num;
        Box::new(
            (0..pose.vertices.len())
                .filter(move |i| !fixed.contains(i))
                .flat_map(move |vertex_i| RandTranslationIter {
                    vertex: vertex_i,
                    diam_x: self.diam_x,
                    diam_y: self.diam_y,
                })
                .take(n),
        )
    }
}
