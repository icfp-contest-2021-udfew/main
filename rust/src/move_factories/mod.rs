pub mod pose_rotations;
pub use pose_rotations::PoseRotations;
pub mod pose_translations;
pub use pose_translations::PoseTranslations;
pub mod rand_translations;
pub use rand_translations::RandTranslations;
pub mod vertex_translations;
pub use vertex_translations::VertexTranslations;
pub mod vertex_valids;
pub use vertex_valids::VertexValids;

use std::{collections::HashSet, fmt::Debug};

use crate::specification::*;

#[derive(Clone, Debug)]
pub enum Move {
    Vertex(usize, Vector),
    Pose(Vector),
    Pointwise(Vec<Vector>),
}

pub trait MoveFactory {
    fn get_moves<'a>(
        &'a mut self,
        _: &Problem,
        _: &Pose,
        _: &'a HashSet<usize>,
    ) -> Box<dyn Iterator<Item = Move> + 'a>;
}

impl Problem {
    pub fn fix_special(&self, pose: &Pose) -> HashSet<usize> {
        let mut on_special = HashSet::new();

        for (i, p) in pose.vertices.iter().enumerate() {
            if self.hole.iter().any(|v| v == p)
                || self.bonuses.iter().any(|b| b.position == *p)
            {
                on_special.insert(i);
            }
        }

        on_special
    }
}

pub struct Chain<F, G>(pub F, pub G);

impl<F: MoveFactory, G: MoveFactory> MoveFactory for Chain<F, G> {
    fn get_moves<'a>(
        &'a mut self,
        problem: &Problem,
        pose: &Pose,
        fixed: &'a HashSet<usize>,
    ) -> Box<dyn Iterator<Item = Move> + 'a> {
        Box::new(
            self.0
                .get_moves(problem, pose, fixed)
                .chain(self.1.get_moves(problem, pose, fixed)),
        )
    }
}

type Vector = [Scalar; 2];

static DIRECTIONS: [Vector; 8] = [
    [0, 1],
    [0, -1],
    [1, 0],
    [-1, 0],
    [1, 1],
    [1, -1],
    [-1, 1],
    [-1, -1],
];

impl Move {
    pub fn apply(&self, pose: &mut Pose) {
        match self {
            Move::Vertex(vertex_i, dir) => {
                pose.vertices[*vertex_i][0] += dir[0];
                pose.vertices[*vertex_i][1] += dir[1];
            }
            Move::Pose(dir) => {
                for vertex in &mut pose.vertices {
                    vertex[0] += dir[0];
                    vertex[1] += dir[1];
                }
            }
            Move::Pointwise(vectors) => {
                for (vertex, vector) in
                    pose.vertices.iter_mut().zip(vectors.iter())
                {
                    vertex[0] += vector[0];
                    vertex[1] += vector[1];
                }
            }
        }
    }
    pub fn revert(&self, pose: &mut Pose) {
        match self {
            Move::Vertex(vertex_i, dir) => {
                pose.vertices[*vertex_i][0] -= dir[0];
                pose.vertices[*vertex_i][1] -= dir[1];
            }
            Move::Pose(dir) => {
                for vertex in &mut pose.vertices {
                    vertex[0] -= dir[0];
                    vertex[1] -= dir[1];
                }
            }
            Move::Pointwise(vectors) => {
                for (vertex, vector) in
                    pose.vertices.iter_mut().zip(vectors.iter())
                {
                    vertex[0] -= vector[0];
                    vertex[1] -= vector[1];
                }
            }
        }
    }
}
