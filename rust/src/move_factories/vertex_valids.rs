use std::iter::Take;

use rand::{thread_rng, Rng};

use super::*;
use crate::{
    geometry::positions::possible_vertex_positions, graph::EdgeLenGraph,
    specification::*,
};

pub struct VertexValids(
    EdgeLenGraph,
    Option<usize>,
    Option<Take<RandomOrder<Move>>>,
);

impl VertexValids {
    pub fn new(
        pose: &Pose,
        problem: &Problem,
        num_per_vertex: Option<usize>,
    ) -> Self {
        VertexValids(problem.graph(pose), num_per_vertex, None)
    }
}

impl MoveFactory for VertexValids {
    fn get_moves<'a>(
        &'a mut self,
        problem: &Problem,
        pose: &Pose,
        fixed: &'a HashSet<usize>,
    ) -> Box<dyn Iterator<Item = Move> + 'a> {
        let moves = pose
            .vertices
            .iter()
            .enumerate()
            .filter(|(i, _)| !fixed.contains(i))
            .map(|(i, v)| {
                let a = possible_vertex_positions(
                    i,
                    &pose,
                    &self.0,
                    problem.epsilon,
                )
                .iter()
                .map(|p| (i, [p[0] - v[0], p[1] - v[1]]))
                .filter(|(_, dir)| dir[0].abs() > 1 || dir[1].abs() > 1)
                .collect::<Vec<_>>();
                //println!("Valids for this vertex: {}", a.len());
                a
            })
            .flatten()
            .map(|(i, dir)| Move::Vertex(i, dir))
            .collect::<Vec<_>>();
        let num = moves.len();
        let to_take = self.1.unwrap_or(num);
        self.2 = Some(RandomOrder(num > to_take, moves).take(to_take));

        Box::new(self.2.as_mut().unwrap())
    }
}

struct RandomOrder<T>(bool, Vec<T>);

impl<T> Iterator for RandomOrder<T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        if self.1.len() == 0 {
            None
        } else {
            let index = if self.0 {
                thread_rng().gen_range(0..self.1.len())
            } else {
                0
            };
            Some(self.1.swap_remove(index))
        }
    }
}
