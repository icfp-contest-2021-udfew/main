use super::*;
use crate::specification::*;

pub struct VertexTranslations(pub Vec<Move>);

impl VertexTranslations {
    pub fn new(problem: &Problem) -> Self {
        VertexTranslations(
            (0..problem.figure.vertices.len())
                .flat_map(|vertex_i| {
                    DIRECTIONS.iter().map(move |d| Move::Vertex(vertex_i, *d))
                })
                .collect(),
        )
    }
}

impl MoveFactory for VertexTranslations {
    fn get_moves<'a>(
        &'a mut self,
        problem: &Problem,
        pose: &Pose,
        fixed: &'a HashSet<usize>,
    ) -> Box<dyn Iterator<Item = Move> + 'a> {
        match pose.broken_leg() {
            Some(_) => {
                let l = problem.figure.vertices.len();
                Box::new(
                    self.0
                        .iter()
                        .filter(move |m| {
                            if let Move::Vertex(i, _) = m {
                                !fixed.contains(i)
                            } else {
                                panic!()
                            }
                        })
                        .cloned()
                        .chain(
                            DIRECTIONS.iter().map(move |d| Move::Vertex(l, *d)),
                        ),
                )
            }
            None => Box::new(self.0.iter().cloned()),
        }
    }
}
