use std::iter::empty;

use super::*;
use crate::geometry::floating::*;
use crate::specification::*;

pub struct PoseRotations(pub usize);

impl MoveFactory for PoseRotations {
    fn get_moves(
        &mut self,
        _: &Problem,
        pose: &Pose,
        fixed: &HashSet<usize>,
    ) -> Box<dyn Iterator<Item = Move>> {
        if fixed.is_empty() && self.0 > 1 {
            let step = 2.0 * PI / self.0 as Floating;
            let vertices = pose.vertices.clone();

            let mut x_min = Scalar::MAX;
            let mut x_max = Scalar::MIN;
            let mut y_min = Scalar::MAX;
            let mut y_max = Scalar::MIN;
            for v in vertices.iter() {
                if v[0] < x_min {
                    x_min = v[0];
                }
                if v[0] > x_max {
                    x_max = v[0];
                }
                if v[1] < y_min {
                    y_min = v[1];
                }
                if v[1] > y_max {
                    y_max = v[1];
                }
            }
            let middle = [(x_max - x_min) / 2, (y_max - y_min) / 2];

            Box::new((0..(self.0 - 1)).scan(0.0, move |angle, _| {
                *angle += step;
                let translations = vertices
                    .iter()
                    .map(|vertex| {
                        let vector = [
                            (vertex[0] - middle[0]) as Floating,
                            (vertex[1] - middle[1]) as Floating,
                        ];
                        let rotated_vector = [
                            vector[0] * Floating::cos(*angle)
                                - vector[1] * Floating::sin(*angle),
                            vector[0] * Floating::sin(*angle)
                                - vector[1] * Floating::cos(*angle),
                        ];
                        [
                            Floating::round(rotated_vector[0] - vector[0])
                                as Scalar,
                            Floating::round(rotated_vector[1] - vector[1])
                                as Scalar,
                        ]
                    })
                    .collect();
                Some(Move::Pointwise(translations))
            }))
        } else {
            Box::new(empty())
        }
    }
}
