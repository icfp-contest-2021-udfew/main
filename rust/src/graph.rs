use crate::{checks::squared_distance, specification::*};

pub type EdgeLenGraph =
    petgraph::stable_graph::StableUnGraph<(), Scalar, usize>;

impl Problem {
    pub fn graph(&self, pose: &Pose) -> EdgeLenGraph {
        EdgeLenGraph::from_edges(pose.edges_for(self).map(|(p0, p1)| {
            (
                p0,
                p1,
                squared_distance(
                    &self.figure.vertices[p0],
                    &self.figure.vertices[p1],
                ),
            )
        }))
    }
}
