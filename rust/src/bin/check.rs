use std::env;
use std::process;

use icfp_contest_mmxxi_udfew_rust::specification::{Pose, Problem};

fn main() {
    let mut args = env::args().skip(1);
    let problem = Problem::parse_file(args.next().unwrap());
    let pose = Pose::parse_file(args.next().unwrap());
    let mut ok = true;
    for e in pose.errors_for(&problem) {
        ok = false;
        eprintln!("{:?}", e);
    }
    process::exit(if ok { 0 } else { 1 })
}
