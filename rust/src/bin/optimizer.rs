use std::collections::HashSet;
use std::io::stdin;
use std::path::PathBuf;
use std::process;
use structopt::StructOpt;

use icfp_contest_mmxxi_udfew_rust::geometry::floating::Floating;
use icfp_contest_mmxxi_udfew_rust::move_factories::*;
use icfp_contest_mmxxi_udfew_rust::optimizers::*;
use icfp_contest_mmxxi_udfew_rust::scorers::*;
use icfp_contest_mmxxi_udfew_rust::solvers::*;
use icfp_contest_mmxxi_udfew_rust::specification::*;

#[derive(Debug, StructOpt)]
#[structopt(name = "optimizer", about = "A solver/optimizer for ICFPC 2021.")]
struct Options {
    /// The "context" file to read.
    ///
    /// The file should contain a JSON object with a problem specification
    /// (under the `problem` key) and a pose to be optimized (under the
    /// `pose` key).
    ///
    /// Conflicts with `problem` and `solution`.
    ///
    /// If neither this option nor conflicting options are specified,
    /// the context will be read from `stdin`.
    #[structopt(
        short,
        long,
        parse(from_os_str),
        conflicts_with_all = &["problem", "pose"]
    )]
    context: Option<PathBuf>,

    /// The problem file to read.
    ///
    /// The file should contain the JSON representation of a problem according
    /// to the specification.
    ///
    /// Conflicts with: `context`.
    #[structopt(long, parse(from_os_str), conflicts_with = "context")]
    problem: Option<PathBuf>,

    /// The pose file to read.
    ///
    /// The file should contain the JSON representation of a pose according
    /// to the specification.
    ///
    /// Conflicts with: `context`.
    ///
    /// If `--problem` is specified bu this argument is omitted, the starting
    /// pose will be used.
    #[structopt(
        long, parse(from_os_str),
        conflicts_with_all = &["context", "solver"]
    )]
    pose: Option<PathBuf>,

    /// Optimizer to use.
    #[structopt(
        short,
        long,
        possible_values = &[
            "exhaustive_descent",
            "simultaneous_descent",
            "annealer",
            "cocktail",
            "cocktail2",
            "cocktail_anneal_first",
        ]
    )]
    algorithm: String,

    /// Optional: Solver to use
    #[structopt(
        short,
        long,
        possible_values = &[
            "spreading_out",
            "random_spread",
            "rotated_clock",
            "rotated_counterclock",
            "rotated_flip",
        ]
    )]
    solver: Option<String>,

    /// Depth to use in the search algorithms.
    #[structopt(short, long, default_value = "1")]
    depth: u32,

    /// Optional: Number of valid moves to use per vertex
    #[structopt(long)]
    valids: Option<usize>,

    /// Fix points that are initially on hole vertices or bonuses
    #[structopt(short, long)]
    fix_special: bool,

    /// Optional: the file into which to write the movie of poses
    #[structopt(short, long, parse(from_os_str))]
    movie_file: Option<PathBuf>,

    /// Whether to continuously write the movie to a file (has no effect if
    /// `movie_file` is not specified).
    #[structopt(short, long)]
    live: bool,

    /// Add more `v`s to print more stuff to `stderr`.
    #[structopt(short, long, parse(from_occurrences))]
    verbose: u8,

    /// Number of random translations to try per vertex
    #[structopt(long, default_value = "0")]
    rand_translations: usize,

    /// Number of rotations to try per iteration, plus one (because the
    /// identity rotation is always skipped)
    #[structopt(long, default_value = "1")]
    rotations: usize,

    /// Scaling factor for the `random_spread` solver.
    #[structopt(long, required_if("solver", "random_spread"))]
    scaling: Option<Floating>,
}

macro_rules! choose_solver {
    ($opt:expr, $o:expr, $pr:expr, $po:expr, $m:expr, $c:expr) => {
        match $opt.solver.as_ref().map(|s| s.as_str()) {
            None => $o.optimize($pr, $po, $m, $c),
            Some("") => $o.optimize($pr, $po, $m, $c),
            Some("spreading_out") => {
                *($po) = SpreadingOut($o).solve($pr, $m, $c);
            }
            Some("random_spread") => {
                *($po) =
                    RandomSpread($o, $opt.scaling.unwrap()).solve($pr, $m, $c);
            }
            Some("rotated_clock") => {
                *($po) =
                    StartRotated(Rotation::Clockwise, $o).solve($pr, $m, $c);
            }
            Some("rotated_counterclock") => {
                *($po) = StartRotated(Rotation::Counterclockwise, $o)
                    .solve($pr, $m, $c);
            }
            Some("rotated_flip") => {
                *($po) = StartRotated(Rotation::Flipped, $o).solve($pr, $m, $c);
            }
            Some(s) => panic!("`{}` is not a solver identifier", s),
        };
    };
}

fn main() {
    let options = Options::from_args();
    let Context { problem, mut pose } = match options.problem {
        Some(p) => {
            let problem = Problem::parse_file(p);
            let pose = match options.pose {
                Some(p) => Pose::parse_file(p),
                None => Pose {
                    vertices: problem.figure.vertices.clone(),
                    bonuses: Vec::new(),
                },
            };
            Context { problem, pose }
        }
        None => match options.context {
            Some(p) => Context::parse_file(p),
            None => Context::parse_read(&mut stdin()),
        },
    };

    let live = options.live;
    let mut cinema = Cinema(options.movie_file.map(|p| (p, Vec::new(), live)));

    let move_factory = Chain(
        PoseRotations(options.rotations),
        Chain(
            RandTranslations::new(options.rand_translations, &problem),
            Chain(
                VertexValids::new(&pose, &problem, options.valids),
                Chain(PoseTranslations(), VertexTranslations::new(&problem)),
            ),
        ),
    );
    //let scorer = WithoutOverstretched((PunishOutside::new(), Dislikes()));
    let mut scorer = (
        PunishOutside::new(&pose, &problem),
        LeftBiased(Overstretch(), Dislikes()),
    );
    let fixed = if options.fix_special {
        if options.verbose > 1 {
            eprintln!("FIXED:\n{:?}", problem.fix_special(&pose));
        }
        problem.fix_special(&pose)
    } else {
        HashSet::new()
    };

    match options.algorithm.as_str() {
        "exhaustive_descent" => {
            let mut optimizer = WithDepth(
                ExhaustiveDescent {
                    move_factory,
                    scorer: scorer.clone(),
                    fixed,
                },
                options.depth,
            );
            choose_solver!(
                options,
                optimizer,
                &problem,
                &mut pose,
                Some(1),
                &mut cinema
            );
        }
        "simultaneous_descent" => {
            let mut optimizer: Vec<(Box<dyn Optimizer>, Option<usize>)> = vec![
                (
                    Box::new(SimultaneousDescent {
                        move_factory,
                        scorer: scorer.clone(),
                        fixed: fixed.clone(),
                    }),
                    None,
                ),
                (
                    Box::new(WithDepth(
                        ExhaustiveDescent {
                            move_factory: Chain(
                                VertexValids::new(
                                    &pose,
                                    &problem,
                                    options.valids,
                                ),
                                Chain(
                                    PoseTranslations(),
                                    VertexTranslations::new(&problem),
                                ),
                            ),
                            scorer: scorer.clone(),
                            fixed: fixed.clone(),
                        },
                        options.depth,
                    )),
                    None,
                ),
            ];
            choose_solver!(
                options,
                optimizer,
                &problem,
                &mut pose,
                Some(1),
                &mut cinema
            );
        }
        "annealer" => {
            if options.verbose > 0 {
                eprintln!(
                    "WARNING: \
                     Using an optimizer that does not make use of `depth`.\
                    "
                );
            }
            let mut optimizer = Annealer::new(0.9, 0.01, 1.0, 0.01);
            choose_solver!(
                options,
                optimizer,
                &problem,
                &mut pose,
                Some(1),
                &mut cinema
            );
        }
        "cocktail" => {
            let mut optimizer: Vec<(Box<dyn Optimizer>, Option<usize>)> = vec![
                (
                    Box::new(SimultaneousDescent {
                        move_factory,
                        scorer: scorer.clone(),
                        fixed: fixed.clone(),
                    }),
                    None,
                ),
                (
                    Box::new(WithDepth(
                        ExhaustiveDescent {
                            move_factory: Chain(
                                VertexValids::new(
                                    &pose,
                                    &problem,
                                    options.valids,
                                ),
                                Chain(
                                    PoseTranslations(),
                                    VertexTranslations::new(&problem),
                                ),
                            ),
                            scorer: scorer.clone(),
                            fixed: fixed.clone(),
                        },
                        options.depth,
                    )),
                    None,
                ),
                (Box::new(Annealer::new(0.9, 0.01, 1.0, 0.01)), None),
            ];
            choose_solver!(
                options,
                optimizer,
                &problem,
                &mut pose,
                None,
                &mut cinema
            );
        }
        "cocktail2" => {
            let mut optimizer: Vec<(Box<dyn Optimizer>, Option<usize>)> = vec![
                (
                    Box::new(SimultaneousDescent {
                        move_factory,
                        scorer: scorer.clone(),
                        fixed: fixed.clone(),
                    }),
                    None,
                ),
                (
                    Box::new(WithDepth(
                        ExhaustiveDescent {
                            move_factory: Chain(
                                VertexValids::new(
                                    &pose,
                                    &problem,
                                    options.valids,
                                ),
                                Chain(
                                    PoseTranslations(),
                                    VertexTranslations::new(&problem),
                                ),
                            ),
                            scorer: scorer.clone(),
                            fixed: fixed.clone(),
                        },
                        options.depth,
                    )),
                    None,
                ),
                (Box::new(Annealer::new(0.9, 0.01, 1.0, 0.01)), None),
            ];
            choose_solver!(
                options,
                optimizer,
                &problem,
                &mut pose,
                None,
                &mut cinema
            );
        }
        "cocktail_anneal_first" => {
            let mut optimizer: Vec<(Box<dyn Optimizer>, Option<usize>)> = vec![
                (Box::new(Annealer::new(0.9, 0.01, 1.0, 0.01)), Some(500)),
                (
                    Box::new(SimultaneousDescent {
                        move_factory,
                        scorer: scorer.clone(),
                        fixed: fixed.clone(),
                    }),
                    None,
                ),
                (
                    Box::new(WithDepth(
                        ExhaustiveDescent {
                            move_factory: Chain(
                                VertexValids::new(
                                    &pose,
                                    &problem,
                                    options.valids,
                                ),
                                Chain(
                                    PoseTranslations(),
                                    VertexTranslations::new(&problem),
                                ),
                            ),
                            scorer: scorer.clone(),
                            fixed: fixed.clone(),
                        },
                        options.depth,
                    )),
                    None,
                ),
            ];
            choose_solver!(
                options,
                optimizer,
                &problem,
                &mut pose,
                None,
                &mut cinema
            )
        }
        s => panic!("`{}` is not an algorithm identifier", s),
    }

    if !options.live {
        cinema.write()
    }

    if options.verbose > 0 {
        eprintln!("SCORE:\n{:?}", scorer.score(&problem, &pose));
    }

    let exit_code = if pose.is_valid_for(&problem) {
        if options.verbose > 0 {
            eprintln!("VALID");
        }
        0
    } else {
        if options.verbose > 0 {
            eprintln!("INVALID");
        }
        1
    };

    println!("{}", pose.dump());

    process::exit(exit_code);
}
