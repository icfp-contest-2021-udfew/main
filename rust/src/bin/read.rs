use std::env;

use icfp_contest_mmxxi_udfew_rust::specification::Context;

fn main() {
    let args: Vec<String> = env::args().collect();

    println!("{:?}", Context::parse_file(&args[1]));
}
