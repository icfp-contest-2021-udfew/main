function populateProblemrow(){
  for(i in problems){
    let link = document.createElement("a");
    link.href = "?id="+i;
    link.classList.add("problemboxLink");
    let box = document.createElement("div");
    box.innerText = i.toString();
    box.classList.add("problembox");
    if(problems[i].solved){
      box.classList.add("solved");
    }
    link.appendChild(box);
    document.getElementById("problemrow").appendChild(link);
  }
}

var pad = 20;
var spring = 0.01;
var friction = 0.9; //factor, multiplied onto speed at every step
var breakdownDistance = 1;
var noiseFactor = 1.1;
//var noiseRange = 50;   //input
var zoomFactor = 1.5;
var vertexRadius=3;
var bonusRadius=6;
var clickVertexRadius=6;
var clickBonusRadius=9;
var clickLineWidth=4;
var snappingThreshold=5;
var crossWidth=3;
var annealingEpsilon;
var searchRadius=50;
//var autoRoundFrequency=100;   //input
//var playMovieFrequency=50;   //input
var crossPalette=["#222288","#c70039", "#ffc300"]; //color scheme for "valid lattice points". First is maximal number of green edges, then count down

function help(){
  console.log("adjustable parameters: vertexRadius, clickVertexRadius, snappingThreshold, crossWidth, crossPalette");
  console.log("recover(id,figure) can be used to recover previously exported solutions");
}

var id = 1;
var intervalID;

function boundingBox(hole,figure, currentFigure){
  if (hole.length === 0){
    console.log("List empty, this shouldn't happen!");
    return;
  }
  let minX = hole[0][0];
  let maxX = hole[0][0];
  let minY = hole[0][1];
  let maxY = hole[0][1];
  for(let i=1; i<hole.length; i++){
    if(hole[i][0]<minX){
      minX=hole[i][0];
    }
    if(hole[i][0]>maxX){
      maxX=hole[i][0];
    }
    if(hole[i][1]<minY){
      minY=hole[i][1];
    }
    if(hole[i][1]>maxY){
      maxY=hole[i][1];
    }
  }
  for(vs of [figure.vertices, currentFigure.vertices]){
    for(let i=0; i<vs.length; i++){
      if(vs[i][0]<minX){
        minX=vs[i][0];
      }
      if(vs[i][0]>maxX){
        maxX=vs[i][0];
      }
      if(vs[i][1]<minY){
        minY=vs[i][1];
      }
      if(vs[i][1]>maxY){
        maxY=vs[i][1];
      }
    }
  }
  return {minX: minX, maxX: maxX, minY: minY, maxY: maxY}
}

function dSquared(v,w){
  let dx=v[0]-w[0];
  let dy=v[1]-w[1];
  return (dx*dx+dy*dy);
}

function getInput(id){
  return document.getElementById(id).value;
}


function toggleButtonInit(button, bool, onChange){
  let state = bool;
  if(state){
      button.classList.add("on");
  }
  button.addEventListener("click",() => {
    if(state){
      state=false;
      button.classList.remove("on");
      if(onChange){
        onChange(false);
      }
    }
    else{
      state=true;
      button.classList.add("on");
      if(onChange){
        onChange(true);
      }
    }
  });
  getState = function(){
    return state;
  };
  setState = function(newState,quiet){
    if(state !== newState){
      if(state){
        state=false;
        button.classList.remove("on");
        if(onChange && !quiet){
          onChange(false);
        }
      }
      else{
        state=true;
        button.classList.add("on");
        if(onChange && !quiet){
          onChange(true);
        }
      }
    }
  };
  return {get: getState, set: setState};
}

function load(hole, bonuses, initialFigure, currentFigure, epsilon){
  document.title ="Problem " + id + " udfew visualiser"
  if(bonuses===null){
    bonuses=[];
  }
  annealingEpsilon = epsilon;
  let canvas = document.getElementById('canvas');
  let box;

  let scale;
  let x0;
  let y0;
  let tooltip = null;
  let selected = [];
  let snapped = [];

  let beginPath = null;
  let endPath = null;
  let updatePath = null;

  let matching = [];



  let speed = [];

  let frames = [];
  let currentFrame = 0;

  let playMovieCounter = 0;
  let autoRoundCounter = 0;

  let clipping = toggleButtonInit(document.getElementById("clippingButton"), false);
  let findHolePath = toggleButtonInit(document.getElementById("findHoleButton"), false, (newState)=>{
    if(!newState){
      beginPath = null;
      endPath = null;
      updatePath = null;
      for(let i=0; i<initialFigure.edges.length; i++){
        matching[i]=false;
      }
    }
  });
  let snapping = toggleButtonInit(document.getElementById("snappingButton"), false, (newState)=>{
    if(!newState){
      for(let i=0; i<snapped.length; i++){
        snapped[i] = false;
      }
    }
  });
  let highlight = toggleButtonInit(document.getElementById("highlightButton"), false);
  let keepAnnealing = toggleButtonInit(document.getElementById("keepAnnealingButton"), true);
  let autoRound = toggleButtonInit(document.getElementById("autoRoundButton"), true);
  let noise = toggleButtonInit(document.getElementById("noiseButton"), false);
  let annealing = toggleButtonInit(document.getElementById("annealingButton"), false, (newState)=>{
    if(!newState){
      for(let i=0; i<figure.vertices.length; i++){
        speed[i]=[0,0]
      }
      round();
    }
  });
  let tooltips = toggleButtonInit(document.getElementById("tooltipButton"), false);
  let showValidPts = toggleButtonInit(document.getElementById("validButton"), false);
  let globalistSource = "";
  let globalist = toggleButtonInit(document.getElementById("globalistButton"), false, (newState)=>{
    if(newState){
      globalistSource = parseInt(prompt("From where do you have the GLOBALIST bonus?"));
    }
  });
  let wallhackSource = "";
  let wallhack = toggleButtonInit(document.getElementById("wallhackButton"), false, (newState)=>{
    if(newState){
      wallhackSource = parseInt(prompt("From where do you have the WALLHACK bonus?"));
    }
  });
  breakALegSource = "";
  breakALegEdge= null;
  let breakALeg = toggleButtonInit(document.getElementById("breakALegButton"), false, (newState)=>{
    if(newState){
      breakALegSource = parseInt(prompt("From where do you have the BREAK_A_LEG bonus?"));
      document.getElementById("breakALegButton").innerText="Break a leg:\n click on edge to break!";
    }else{
      undoBreakALeg();
    }
  });
  let superflexSource = "";
  let superflex = toggleButtonInit(document.getElementById("superflexButton"), false, (newState)=>{
    if(newState){
      superflexSource = parseInt(prompt("From where do you have the SUPERFLEX bonus?"));
    }
  });
  let playMovie = toggleButtonInit(document.getElementById("playButton"), false);

  let figure = {edges: [], vertices: []};
  for(let i=0;i<currentFigure.edges.length; i++){
    figure.edges.push([currentFigure.edges[i][0], currentFigure.edges[i][1]]);
    matching.push(false);
  }
  for(let i=0;i<currentFigure.vertices.length; i++){
    figure.vertices.push([currentFigure.vertices[i][0], currentFigure.vertices[i][1]]);
    selected.push(false);
    snapped.push(false);
    speed.push([0,0]);
  }
  let anchorPos = null;
  let anchor = null;
  let boxSelect = null;

  let zoomingIn = false;

  let computeViewport = function(){
    box = boundingBox(hole, initialFigure, figure);
    scale = Math.min((canvas.width - pad) / (box.maxX - box.minX),
                      (canvas.height - pad)/ (box.maxY - box.minY))
    x0 = (canvas.width - (box.minX + box.maxX)*scale ) / 2
    y0 = (canvas.height - (box.minY + box.maxY)*scale) / 2
  }

  let worldToScreen = function(v){
    return([v[0]*scale + x0, v[1]*scale+y0]);
  }

  let screenToWorld = function(v){
    return([Math.round((v[0]-x0)/scale), Math.round((v[1]-y0)/scale)]);
  }

  let computeDislikes = function(){
    let dislikes = 0;
    for(let i=0; i<hole.length; i++){
      let dist = dSquared(figure.vertices[0], hole[i]);
      for(let j=0; j<figure.vertices.length; j++){
        let distNew = dSquared(figure.vertices[j], hole[i]);
        if(distNew < dist){
          dist=distNew;
        }
      }
      dislikes = dislikes + dist;
    }
    document.getElementById('dislikeCounter').innerText = dislikes;
  }

  let computeGlobalistStretch= function(){
    let stretch = 0;
    for(let i=0; i<figure.edges.length; i++){
      let sIndex = figure.edges[i][0];
      let tIndex = figure.edges[i][1];
      let dNow = dSquared(figure.vertices[sIndex], figure.vertices[tIndex]);
      let dOriginal = dSquared(initialFigure.vertices[sIndex], initialFigure.vertices[tIndex]);
      stretch += Math.abs(dNow/dOriginal - 1);
    }
    let budget = epsilon*(figure.edges.length - !!breakALeg.get())/1000000;
    document.getElementById('globalistStretchCounter').innerText = stretch;
    document.getElementById('globalistStretchBudget').innerText = budget;
    if(stretch > budget) {
      document.getElementById('globalistStatus').className = 'badStatus';
    } else {
      document.getElementById('globalistStatus').className = 'goodStatus';
    }
  }

  let movable = function(index){
    return (!((snapping.get() && snapped[index]) || selected[index]));
  }

  // i is the index in edges.
  // we add a new vertex v', replace the edge [s,t] by [s,v']
  // and append [v', t]
  // Hopefully this will work!
  function doBreakALeg(i) {
    let s = figure.edges[i][0];
    let t = figure.edges[i][1];
    let v = figure.vertices.length;
    let xold = (initialFigure.vertices[s][0] + initialFigure.vertices[t][0])/2;
    let yold = (initialFigure.vertices[s][1] + initialFigure.vertices[t][1])/2;
    let xnew = (figure.vertices[s][0] + figure.vertices[t][0])/2;
    let ynew = (figure.vertices[s][1] + figure.vertices[t][1])/2;
    let l = figure.edges.length;
    figure.edges[i][1] = v;
    figure.edges[l] = [v,t];
    initialFigure.edges[i][1] = v;
    initialFigure.edges[l]= [v,t];
    initialFigure.vertices[v]=[xold,yold];
    figure.vertices[v]=[xnew,ynew];
    breakALegEdge = {edgeIndex: i, s: s, t: t, v: v};
    document.getElementById('breakALegButton').innerText= "Break a leg: ["+s+", "+t+"]";
    round();
  }


  function undoBreakALeg() {
    if(breakALegEdge === null)
      return;
    let s = breakALegEdge.s;
    let t = breakALegEdge.t;
    figure.edges.pop();
    initialFigure.edges.pop();
    initialFigure.vertices.pop();
    figure.vertices.pop();
    figure.edges[breakALegEdge.edgeIndex] = [s,t];
    initialFigure.edges[breakALegEdge.edgeIndex] = [s,t];
    breakALegEdge = null;
    document.getElementById('breakALegButton').innerText= "Break a leg";
  }

  let drawFrameCount = function(){
    document.getElementById('frameCounter').innerText = currentFrame;
    document.getElementById('frameCardinality').innerText = frames.length;
  }

  let initFindPath = function(dSq){
    let matches=[];
    let neighbours=[];
    for(let i=0; i<figure.vertices.length; i++){
      neighbours.push([]);
    }
    for(let i=0; i<figure.edges.length; i++){
      let index0 = figure.edges[i][0];
      let index1 = figure.edges[i][1];
      let dOriginal = dSquared(initialFigure.vertices[index0], initialFigure.vertices[index1]);
      neighbours[index0].push({to: index1, via: i, d: dOriginal});
      neighbours[index1].push({to: index0, via: i, d: dOriginal});
      if(Math.abs(dSq - dOriginal)*1000000 <= epsilon*dOriginal){
        matches.push([index0,index1]);
        matches.push([index1,index0]);
        matching[i]=true;
      }
    }
    if(matches.length == 0){
      findHolePath.set(false);
      return;
    }
    let update = function(nextD,beginning){
      for(let i=0; i<figure.edges.lenght; i++){
        matching[i]=false;
      }
      let newMatches = [];
      for(let i=0; i<matches.length; i++){
        if(beginning){
          let vertex = matches[i][0];
          for(let j=0; j<neighbours[vertex].length; j++){
            if(Math.abs(nextD - neighbours[vertex][j].d)*1000000 <= epsilon*neighbours[vertex][j].d){
              let newMatch = []; 
              newMatch.push(neighbours[vertex][j].to);
              matching[neighbours[vertex][j].via]=true;
              for(let k=0; k<matches[i].length; k++){
                newMatch.push(matches[i][k]);
                matching[matches[i][k]]=true;
              }
              newMatches.push(newMatch);
            }
          }
        }
        else{
          let vertex = matches[i][matches[i].length - 1];
          for(let j=0; j<neighbours[vertex].length; j++){
            if(Math.abs(nextD - neighbours[vertex][j].d)*1000000 <= epsilon*neighbours[vertex][j].d){
              let newMatch = []; 
              for(let k=0; k<matches[i].length; k++){
                newMatch.push(matches[i][k]);
                matching[matches[i][k]]=true;
              }
              newMatch.push(neighbours[vertex][j].to);
              matching[neighbours[vertex][j].via]=true;
              newMatches.push(newMatch);
            }
          }
        }
      }
      matches = newMatches;
      if(matches.length == 1){
        for(let i=0; i<matches[0].length; i++){
          let holeI = (beginPath + i) % hole.length;
          figure.vertices[matches[0][i]] = [hole[holeI][0], hole[holeI][1]];
          if(snapping.get()){
            snapped[matches[0][i]] = true;
          }
        }
      }
      if(matches.length <= 1){
        findHolePath.set(false);
      }
    }
    return update;
  }




  let getMousePos = function(event){
    let rect = canvas.getBoundingClientRect();
    return{
      x: event.clientX - rect.left,
      y: event.clientY - rect.top
    }
  }

  let touchesPoint = function(radius, pointVect, mouseVect){
    let offset = [pointVect[0]-mouseVect[0], pointVect[1]-mouseVect[1]];
    let lSq = offset[0]*offset[0]+offset[1]*offset[1];
    return (lSq <= radius*radius);
  }

  let touchesLine = function(radius, beginVect, endVect, mouseVect){
    let lineVect = [endVect[0]-beginVect[0], endVect[1]-beginVect[1]];
    let lSq = lineVect[0]*lineVect[0]+lineVect[1]*lineVect[1];
    if(lSq === 0){
      return touchesPoint(radius, beginVect, mouseVect);
    }
    else{
      let offset = [mouseVect[0]-beginVect[0], mouseVect[1]-beginVect[1]];
      let scalar = offset[0]*lineVect[0] + offset[1]*lineVect[1];
      let det = offset[0]*lineVect[1] - offset[1]*lineVect[0];
      let orthogonally = (scalar >= -Math.sqrt(lSq)*radius)
           && (scalar <= lSq + Math.sqrt(lSq)*radius);
      let parallely = (Math.abs(det) <= Math.sqrt(lSq)*radius);
      return (orthogonally && parallely);
    }
  }

  let mousedown = function(event){
    document.getElementById("seekButton").classList.remove("invalid");
    let pos = getMousePos(event);
    if(zoomingIn){
      scale = scale*zoomFactor;
      //scale * worldClickX + x0 = screenClickX, i.e. scale * worldClickX = screenClickX - x0.
      //after zoom: zoom * scale * worldClickX = screenClickX - x0', since mouse position & world position we pointed at should stay fixed.
      // so (screenClickX - x0') = zoom * (screenClickX - x0) 
      // so x0' = = (1-zoom)*screenClickX + zoom*x0
      // 
      // x0 = zoomFactor*x0 - (zoomFactor-1)*pos.x;
      // y0 = zoomFactor*y0 - (zoomFactor-1)*pos.y;
      // 
      // if I want instead that zoom * scale * wX + x0' = cW/2, scale * wX + x0 = cX,
      //   cW/2 - x0' = zoom * (cX - x0);
      //   x0' = cW/2 - zoom * (cX - x0);
      x0 = canvas.width/2 - zoomFactor * (pos.x - x0);
      y0 = canvas.height/2 - zoomFactor * (pos.y - y0);
      canvas.classList.remove("zooming");
      zoomingIn = false;
      return;
    }
    if(findHolePath.get()){
      for(let i=0; i<hole.length; i++){
        let v0 = hole[i];
        let j = (i+1) % (hole.length);
        let v1 = hole[(i+1) % (hole.length)]
        if(touchesLine(clickLineWidth, worldToScreen(v0), worldToScreen(v1), [pos.x,pos.y])){
          if(beginPath === null){
            beginPath = i;
            endPath = j;
            updatePath = initFindPath(dSquared(v0,v1));
          }
          else if (j == beginPath){
            beginPath = i;
            updatePath(dSquared(v0,v1),true);
          }
          else if (i == endPath){
            endPath = j;
            updatePath(dSquared(v0,v1),false);
          }
        }
      }
    }
    if(event.ctrlKey){
      boxSelect = {x0: pos.x, y0: pos.y, x1: pos.x, y1: pos.y};
    } else if(breakALeg.get() && breakALegEdge === null) {
      for(let i=0; i<figure.edges.length; i++){
        let sIndex = figure.edges[i][0]
        let tIndex = figure.edges[i][1]

        if(touchesLine(clickLineWidth,
             worldToScreen(figure.vertices[sIndex]),
             worldToScreen(figure.vertices[tIndex]),
             [pos.x, pos.y])){
          doBreakALeg(i);
          return;
        }
      }
    }
    else{
      for(let i=0; i<figure.vertices.length; i++){
        if(touchesPoint(clickVertexRadius, worldToScreen(figure.vertices[i]), [pos.x,pos.y])){
          anchor = i;
          if(!selected[i]){
            deselect();
            selected[i]=true;
          }
          anchorPos={x: figure.vertices[i][0], y:figure.vertices[i][1]};
          canvas.classList.add("dragging");
          return;
        }
      }
    }
  }


  let mousemove = function(event){
    let pos = getMousePos(event);
    if(anchor !== null){
      let posWorld = screenToWorld([pos.x,pos.y]);
      let snap = false;
      let bestSnap = null;
      if(snapping.get()){
        for(let i=0; i<hole.length; i++){
          if(dSquared(posWorld, hole[i]) < snappingThreshold * snappingThreshold){
            if(!bestSnap || dSquared(posWorld,hole[i]) < dSquared(posWorld,bestSnap)) {
              bestSnap = hole[i];
              snap=true;
            }
          }
        }
        for(let i=0; i<bonuses.length; i++){
          if(dSquared(posWorld, bonuses[i].position) < snappingThreshold * snappingThreshold){
            if(!bestSnap || dSquared(posWorld,bonuses[i].position) < dSquared(posWorld,bestSnap)) {
              bestSnap = bonuses[i].position;
              snap=true;
            }
          }
        }
        if(snap)
          posWorld = bestSnap;
      }
      vMove = [posWorld[0] - anchorPos.x,
                posWorld[1] - anchorPos.y]

      let newVertices = [];
      for(let i=0; i<figure.vertices.length; i++){
        let vOld = figure.vertices[i];
        if(selected[i]){
          newVertices.push([vOld[0]+vMove[0],vOld[1]+vMove[1]]);
        }
        else{
          newVertices.push(vOld);
        }
      }
      
      if(clipping.get()){
        //for each edge incident to selected vertices, check whether it becomes red.
        for(let i=0; i<figure.edges.length; i++){
          let moved
          let other;
          if(selected[figure.edges[i][0]] && !selected[figure.edges[i][1]]){
            moved = figure.edges[i][0];
            other = figure.edges[i][1];
          }
          else if(selected[figure.edges[i][1]] && !selected[figure.edges[i][0]]){
            moved = figure.edges[i][1];
            other = figure.edges[i][0];
          }
          else{
            continue;
          }
          let dOld = dSquared(figure.vertices[moved], figure.vertices[other]);
          let dOriginal = dSquared(initialFigure.vertices[moved], initialFigure.vertices[other]);
          let dNew = dSquared(newVertices[moved], figure.vertices[other]);
          if(Math.abs(dNew - dOriginal)*1000000 > dOriginal*epsilon && Math.abs(dOld - dOriginal)*1000000 <= epsilon*dOriginal){
            return;
          }
        }
      }
      figure.vertices = newVertices;
      anchorPos.x = posWorld[0];
      anchorPos.y = posWorld[1];
      snapped[anchor]=snap;

      computeDislikes();
      computeGlobalistStretch();
    }
    else if(boxSelect !== null){
      boxSelect.x1 = pos.x;
      boxSelect.y1 = pos.y;
    }
    else if(tooltips.get()){
      tooltipText = "";
      for(let i=0; i<figure.vertices.length; i++){
        let v = figure.vertices[i];
        if(touchesPoint(clickVertexRadius, worldToScreen(v), [pos.x, pos.y])){
          text = "vertex " + i + ": (" /*why the long face?*/ + v[0] + "," + v[1] + ")";
          tooltipText += text + "\n";
        }
      }
      for(let i=0; i<bonuses.length; i++){
        let b = bonuses[i];
        let v = b.position;
        if(touchesPoint(clickBonusRadius, worldToScreen(v), [pos.x,pos.y])){
          text = "bonus " + b.bonus + " for problem "  + b.problem;
          tooltipText += text + "\n";
        }
      }
      for(let i=0; i<figure.edges.length; i++){
        let sIndex = figure.edges[i][0]
        let tIndex = figure.edges[i][1]
        
        if(touchesLine(clickLineWidth,
             worldToScreen(figure.vertices[sIndex]),
             worldToScreen(figure.vertices[tIndex]),
             [pos.x,pos.y])){
          let dNow = dSquared(figure.vertices[sIndex], figure.vertices[tIndex]);
          let dOriginal = dSquared(initialFigure.vertices[sIndex], initialFigure.vertices[tIndex]);
          let dMin = Math.ceil(dOriginal * (1 - epsilon/1000000));
          let dMax = Math.floor(dOriginal * (1 + epsilon/1000000));
          let text = "edge ["+sIndex+","+tIndex+"] now: " + dNow + " min: "+ dMin+" max: " + dMax;
          tooltipText += text + "\n";
        }
      }
      for(let i=0; i<hole.length; i++){
        v0 = hole[i]
        let v1;
        if(i+1 === hole.length){
          v1 = hole[0];
        } 
        else{
          v1 = hole[i+1];
        }
        if(touchesLine(clickLineWidth,
             worldToScreen(v0),
             worldToScreen(v1),
             [pos.x,pos.y])){
          let dSq = dSquared(v1,v0);
          let text = "hole edge: " + dSq;
          tooltipText += text + "\n";
        }
      }
      if(tooltipText === ""){
        tooltip = null;
      }
      else{
        tooltip = {x: pos.x, y: pos.y, text: tooltipText};
      }
    }
  }

  let bump = function(bumpBy){
    bumping=[];
    for(let i=0; i<figure.vertices.length; i++){
      bumping.push(false);
    }
    for(let i=0; i<figure.edges.length; i++){
      let sIndex = figure.edges[i][0];
      let tIndex = figure.edges[i][1];
      let v0 = figure.vertices[sIndex];
      let v1 = figure.vertices[tIndex];
      let dCurrent = dSquared(v0, v1);
      let dOriginal = dSquared(initialFigure.vertices[sIndex], initialFigure.vertices[tIndex]);
      if(Math.abs(dCurrent-dOriginal)*1000000 > annealingEpsilon*dOriginal){
        bumping[sIndex]=true;
        bumping[tIndex]=true;
      }
    }
    for(let i=0; i<figure.vertices.length; i++){
      if(bumping[i] && movable(i)){
        let vX = figure.vertices[i][0] + Math.floor(Math.random() * (2*bumpBy + 1)) - bumpBy;
        let vY = figure.vertices[i][1] + Math.floor(Math.random() * (2*bumpBy + 1)) - bumpBy;
        figure.vertices[i] = [vX,vY];
      }
    }
  }

  let select = function(){
    for(let i=0; i<figure.vertices.length; i++){
      let v=figure.vertices[i];
      let screenPos = worldToScreen(v);
      let minX=boxSelect.x0;
      let maxX=boxSelect.x1;
      if(minX > maxX ){
        minX=boxSelect.x1;
        maxX=boxSelect.x0;
      }
      let minY=boxSelect.y0;
      let maxY=boxSelect.y1;
      if(minY > maxY ){
        minY=boxSelect.y1;
        maxY=boxSelect.y0;
      }
      if((screenPos[0] <= maxX+5 && screenPos[0] >= minX-5)   //extra margin guarantees that control+click selects an additional vertex
        && (screenPos[1] <= maxY+5 && screenPos[1] >= minY-5)){
        selected[i]=true;
      }
         
    }
  }
  let deselect = function(){
    for(let i=0;i<selected.length;i++){
      selected[i]=false;
    }
  }

  let mouseup = function(){
    if(anchor !== null){
      canvas.classList.remove("dragging");
      anchor=null;
      anchorPos=null;
    }
    else if(boxSelect !== null){
      select();
      boxSelect = null;
    }
    else{
      deselect();
    }
  } 

  let round=function(){
      for(let i=0; i<figure.vertices.length; i++){
        figure.vertices[i] = [Math.round(figure.vertices[i][0]), Math.round(figure.vertices[i][1])];
      }
      computeDislikes();
      computeGlobalistStretch();
  }

  let move = function(){
    document.getElementById("seekButton").classList.remove("invalid");
    let accel=[];
    for(let i=0; i<figure.vertices.length; i++){
      accel.push([0,0]);
    }
    let allGreen = true;

    let worstEdge = null;
    let worstEdgeD = null;
    if(superflex.get()){
      for(let i=0; i<figure.edges.length; i++){
        let sIndex = figure.edges[i][0];
        let tIndex = figure.edges[i][1];
        let v0 = figure.vertices[sIndex];
        let v1 = figure.vertices[tIndex];
        let dCurrent = dSquared(v0, v1);
        if(worstEdgeD === null || worstEdgeD <= dCurrent){
          worstEdge = i;
          worstEdgeD = dCurrent;
        }
      }
    }

    for(let i=0; i<figure.edges.length; i++){
      let sIndex = figure.edges[i][0];
      let tIndex = figure.edges[i][1];
      let v0 = figure.vertices[sIndex];
      let v1 = figure.vertices[tIndex];
      let dCurrent = dSquared(v0, v1);
      let dOriginal = dSquared(initialFigure.vertices[sIndex], initialFigure.vertices[tIndex]);
      if(i==worstEdge || (Math.abs(dCurrent-dOriginal)*1000000 <= annealingEpsilon*dOriginal)){
        //worstEdge is only set if superflex active!
        continue;
      }
      else{
        allGreen = false;
      }
      let displacement = Math.sqrt(dCurrent) - Math.sqrt(dOriginal);
      let vX = (v1[0]-v0[0]);
      let vY = (v1[1]-v0[1]);
      let len = Math.sqrt(vX*vX + vY*vY);
      let noiseRange = parseInt(getInput("noiseRange"));
      if(Math.abs(dCurrent-dOriginal)*1000000 <= noiseFactor * epsilon * dOriginal){ //if displacement is small, but edge is not green yet
         vX = Math.floor(Math.random() * (2*noiseRange+1)) - noiseRange;
         vY = Math.floor(Math.random() * (2*noiseRange+1)) - noiseRange;
      }
      let aX;
      let aY;
      if(len <= breakdownDistance){
        aX = breakdownDistance * (Math.floor(Math.random() * 3) - 1);
        aY = breakdownDistance * (Math.floor(Math.random() * 3) - 1);
      }
      else{
        //random direction in a small ball if they get too close
        aX = spring*vX*displacement/len;
        aY = spring*vY*displacement/len;
      }

      accel[sIndex] = [accel[sIndex][0] + aX, accel[sIndex][1] + aY];
      accel[tIndex] = [accel[tIndex][0] - aX, accel[tIndex][1] - aY];
    }
    if(allGreen && !keepAnnealing.get()){
      annealing.set(false);
    }
    else{
      for(let i=0; i<figure.vertices.length; i++){
        if(movable(i)){
          speed[i] = [(speed[i][0]+accel[i][0])*friction, (speed[i][1]+accel[i][1])*friction];
          figure.vertices[i] = [figure.vertices[i][0]+speed[i][0], figure.vertices[i][1]+speed[i][1]];
        }
      }
    }
    if(autoRound.get()){
      autoRoundCounter++;
      if(autoRoundCounter >= 10*parseInt(getInput("autoRoundFrequency"))){
        round();
        autoRoundCounter=0;
        bump(getInput("autoBump"));
      }
    }

  }

  let nextFrame = function(){
    if(frames.length == 0)
      return;
    currentFrame++;
    if (currentFrame >= frames.length)
      currentFrame = 0;
    gotoFrame(currentFrame);
  }

  let draw = function(){
    if(playMovie.get()){
      playMovieCounter++;
      if(playMovieCounter >= parseInt(getInput("playMovieFrequency"))){
        nextFrame();
        playMovieCounter=0;
      }
    }
    else if(annealing.get()){
      move();
    }
    let ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);



    //draw hole
    for(let i=0; i<hole.length; i++){
      ctx.beginPath();
      let screenPos0 = worldToScreen(hole[i]);
      let j = (i+1) % hole.length;
      ctx.moveTo(screenPos0[0], screenPos0[1]);
      ctx.lineWidth=3;
      ctx.setLineDash([]);
      if((beginPath !== null) &&
        ((i >= beginPath && i < endPath) 
        || (endPath <= beginPath && beginPath <= i)
        || (i < endPath && endPath <= beginPath))){
        ctx.strokeStyle="#a432a8";
      }
      else{
        ctx.strokeStyle="#000000";
      }
      let screenPos1 = worldToScreen(hole[(i+1) % hole.length]);
      ctx.lineTo(screenPos1[0], screenPos1[1]);
      ctx.stroke();
    }


    //highlight hole corners if active
    if(highlight.get()){
      for(let i=0; i<hole.length; i++){
        ctx.beginPath();
        ctx.fillStyle="#FF0000";
        let screenPos = worldToScreen(hole[i]);
        ctx.arc(screenPos[0], screenPos[1],
                 bonusRadius, 0, 2*Math.PI);
        ctx.fill();
      }
    }

    //we use this loop also to collect info for valid lattice pts
    let distanceConstraints=[];

    for(let i=0; i<bonuses.length; i++){
      let v = bonuses[i].position;
      ctx.beginPath();
      if(bonuses[i].bonus === "GLOBALIST"){
        ctx.fillStyle="#FFFF00";
      }
      else if(bonuses[i].bonus === "BREAK_A_LEG"){
        ctx.fillStyle="#0000ff";
      }
      if(bonuses[i].bonus === "WALLHACK"){
        ctx.fillStyle="#ffa500";
      }
      if(bonuses[i].bonus === "SUPERFLEX"){
        ctx.fillStyle="#00ffff"; 
      }
      let screenPos = worldToScreen(v);
      ctx.arc(screenPos[0], screenPos[1],
               bonusRadius, 0, 2*Math.PI);
      ctx.fill();
    }

    //draw edges
    for(let i=0; i<figure.edges.length; i++){
      ctx.beginPath();
      let index0 = figure.edges[i][0];
      let index1 = figure.edges[i][1];
      let v0 = figure.vertices[index0];
      let v1 = figure.vertices[index1];
      let screenPos0 = worldToScreen(v0);
      let screenPos1 = worldToScreen(v1);
      ctx.moveTo(screenPos0[0], screenPos0[1]);
      ctx.lineTo(screenPos1[0], screenPos1[1]);
      
      let v0Orig = initialFigure.vertices[initialFigure.edges[i][0]];
      let v1Orig = initialFigure.vertices[initialFigure.edges[i][1]];

      let dOrig = dSquared(v0Orig, v1Orig);
      let dNow = dSquared(v0, v1);

      if(findHolePath.get()){
        if(matching[i]){
          ctx.strokeStyle="#fc0390";
        }
        else{
          ctx.strokeStyle="#aaaaaa";
        }
      }
      else if(1000000*(dNow - dOrig) < -epsilon*dOrig){
        ctx.strokeStyle="#880000";
      }
      else if(1000000*(dNow - dOrig) > epsilon*dOrig){
        ctx.strokeStyle="#FF2222";
      }
      else{
        ctx.strokeStyle="#00FF00";
      }
      ctx.stroke();

      if(showValidPts.get() && anchor === index0){
        distanceConstraints.push({x:v1[0], y:v1[1], d: dOrig}); //length constraint comes from original length!
      }

      if(showValidPts.get() && anchor === index1){
        distanceConstraints.push({x:v0[0], y:v0[1], d: dOrig});
      }
    }
    for(let i=0; i<figure.vertices.length; i++){
      ctx.beginPath();
      if(selected[i]){
        ctx.fillStyle="#FF0000";
      }
      else if(snapping.get() && snapped[i]){
        ctx.fillStyle="#000000";
      }
      else {
        ctx.fillStyle="#0000FF";
      }
      let screenPos = worldToScreen(figure.vertices[i]);
      ctx.arc(screenPos[0], screenPos[1],
               vertexRadius, 0, 2*Math.PI);
      ctx.fill();
    }

    //drawing tooltip
    if(tooltips.get() && tooltip !== null && anchor === null){
      let tt=document.getElementById("tooltip");
      let rect = canvas.getBoundingClientRect();
      tt.style.left= (tooltip.x + rect.left+10)+"px";
      tt.style.top= (tooltip.y + rect.top+10)+"px";
      tt.innerText=tooltip.text;
      tt.classList.remove("hidden");
    }
    else{
      document.getElementById("tooltip").classList.add("hidden");
    }

    //drawing valid lattice points
    if(distanceConstraints.length !== 0){
      validPts = [];
      let minX = box.minX; 
      let maxX = box.maxX;
      let minY = box.minY;
      let maxY = box.maxY;
      for(let x=minX; x<=maxX; x++){
        for(let y=minY; y<=maxY; y++){
          satisfiedNumber = 0;
          for(let i=0; i<distanceConstraints.length; i++){
            let dxy = dSquared([x,y],[distanceConstraints[i].x, distanceConstraints[i].y]);
            if(Math.abs(dxy-distanceConstraints[i].d)*1000000 <= epsilon*distanceConstraints[i].d){
              satisfiedNumber = satisfiedNumber + 1;
            }
          }
          if(satisfiedNumber > 0){
            ctx.beginPath();
            let colorIndex = distanceConstraints.length - satisfiedNumber;
            if(colorIndex >= crossPalette.length){
              colorIndex = crossPalette.length - 1;
            }
            ctx.strokeStyle=crossPalette[colorIndex];
            if(colorIndex === 0){
              ctx.lineWidth=2;
            }
            else{
              ctx.lineWidth=1;
            }
            let xScreen = scale*x + x0;
            let yScreen = scale*y + y0;
            ctx.moveTo(xScreen-crossWidth,yScreen);
            ctx.lineTo(xScreen+crossWidth,yScreen);
            ctx.moveTo(xScreen,yScreen-crossWidth);
            ctx.lineTo(xScreen,yScreen+crossWidth);
            ctx.stroke();
          }
        }
      }
    }

    if(boxSelect !== null){
      ctx.beginPath();
      ctx.strokeStyle="#0bc6e3";
      ctx.lineWidth=1;
      ctx.setLineDash([5, 5]);
      ctx.rect(boxSelect.x0, boxSelect.y0, boxSelect.x1-boxSelect.x0, boxSelect.y1-boxSelect.y0);
      ctx.stroke();
    }
  }

  function setPose(pose) {
    let newGlobalist = false;
    let newGlobalistSource = null;
    let newBreakALeg = false;
    let newBreakALegSource= null;
    let newBreakALegEdge = null;
    let newWallhack = false;
    let newWallhackSource = null
    let newSuperflex = false;
    let newSuperflexSource = null
    // Reset bonuses in order not to break things
    breakALeg.set(false);
    globalist.set(false);
    wallhack.set(false);
    superflex.set(false);
    let bonuses = [];
    if(pose.bonuses)
      bonuses = pose.bonuses;
    // See what bonuses are active in new pose
    for(let i=0; i<bonuses.length; i++) {
      if(bonuses[i].bonus == "GLOBALIST") {
        newGlobalist = true;
        newGlobalistSource = bonuses[i].problem;
      }
      if(bonuses[i].bonus == "BREAK_A_LEG") {
        newBreakALeg= true;
        newBreakALegSource= bonuses[i].problem;
        newBreakALegEdge = bonuses[i].edge; 
      }
      if(bonuses[i].bonus == "WALLHACK") {
        newWallhack= true;
        newWallhackSource= bonuses[i].problem;
      }
      if(bonuses[i].bonus == "SUPERFLEX") {
        newSuperflex= true;
        newSuperflexSource= bonuses[i].problem;
      }
    }

    if(newGlobalist) {
      globalist.set(true, true); // silent update
      globalistSource = newGlobalistSource;
    }

    if(newWallhack) {
      wallhack.set(true, true); // silent update
      wallhackSource = newWallhackSource;
    }

    if(newSuperflex) {
      superflex.set(true, true); // silent update
      superflexSource = newSuperflexSource;
    }

    if(newBreakALeg) {
      let s = newBreakALegEdge[0];
      let t = newBreakALegEdge[1];
      let edgeIndex;
      for(let i=0; i<figure.edges.length; i++) {
        if ((figure.edges[i][0] == s && figure.edges[i][1] == t)
          ||(figure.edges[i][1] == s && figure.edges[i][0] == t))
          edgeIndex = i;
      }
      breakALeg.set(true,true);
      breakALegSource=newBreakALegSource;
      doBreakALeg(edgeIndex);
    }

    if(figure.vertices.length != pose.vertices.length) {
      alert("Import vertex length does not match up! Abort import\n");
      return;
    }
    for(let i=0; i<figure.vertices.length; i++){
      figure.vertices[i][0] = pose.vertices[i][0];
      figure.vertices[i][1] = pose.vertices[i][1];
      snapped[i]=false;
      selected[i]=false;
      if(snapping.get()){
        for(let j=0; j<hole.length; j++){
          if(pose.vertices[i][0] == hole[j][0] && pose.vertices[i][1] == hole[j][1]){
            snapped[i]=true;
            break;
          }
        }
        for(let j=0; j<problems[id].bonuses.length; j++){
          let pos = problems[id].bonuses[j].position;
          if(pose.vertices[i][0] == pos[0] && pose.vertices[i][1] == pos[1]){
            snapped[i]=true;
            break;
          }
        }
      }
    }
    computeDislikes();
    computeGlobalistStretch();
  }

  function getPose() {
    let bonuses = [];
    let vs = [];
    for(let i=0; i<figure.vertices.length; i++){
      vs[i]=[figure.vertices[i][0],figure.vertices[i][1]];
    }
    if(globalist.get()) {
      bonuses.push({bonus: "GLOBALIST", problem: globalistSource});
    }
    if(breakALeg.get()) {
      bonuses.push({bonus: "BREAK_A_LEG", problem: breakALegSource, edge: [breakALegEdge.s, breakALegEdge.t]});
    }
    if(wallhack.get()) {
      bonuses.push({bonus: "WALLHACK", problem: wallhackSource});
    }
    if(superflex.get()) {
      bonuses.push({bonus: "SUPERFLEX", problem: superflexSource});
    }
    return {vertices:vs, bonuses:bonuses};
  }

  computeDislikes();
  computeGlobalistStretch();
  computeViewport();
  drawFrameCount();
  if(intervalID !== null){
    window.clearInterval(intervalID);
  }
  draw();
  let text = JSON.stringify(getPose());
  document.getElementById("poseText").value=text;
  intervalID=window.setInterval(draw, 10);
  canvas.addEventListener("mousedown", mousedown);
  canvas.addEventListener("mouseup", mouseup);
  canvas.addEventListener("mousemove", mousemove);
  document.getElementById("exportButton").addEventListener("click",() => {
    let evilEdges = [];
    for(let i=0; i<figure.edges.length; i++){
      let sIndex = figure.edges[i][0];
      let tIndex = figure.edges[i][1];
      let v0 = figure.vertices[sIndex];
      let v1 = figure.vertices[tIndex];
      let dCurrent = dSquared(v0, v1);
      let dOriginal = dSquared(initialFigure.vertices[sIndex], initialFigure.vertices[tIndex]);
      if(Math.abs(dCurrent-dOriginal)*1000000 > epsilon*dOriginal){
        evilEdges.push(figure.edges[i]);
      }
    }
    let pose = getPose();
    let text = JSON.stringify(pose);
    document.getElementById("poseText").value=text;
    //TODO: better warning logic
    if(evilEdges.length === 0){
      document.getElementById("export").innerText=text;
      console.log(text);
    }
    else{
      let warning = "There are illegal edges:";
      for(let i=0; i<evilEdges.length; i++){
        warning = warning + " [" + evilEdges[i] +"]";
      }
      document.getElementById("export").innerText=text + "\n" + warning;
    }
  });
  document.getElementById("exportMovieButton").addEventListener("click",() => {
    let text = JSON.stringify(frames);
    document.getElementById("export").innerText=text;
  });
  document.getElementById("saveButton").addEventListener("click",() => {
    let text = JSON.stringify({current: figure, initial: initialFigure});
    let url = "ak.udfew.de/?id=" + id + "&figure=" + btoa(text);
    let textArea = document.createElement("textarea");
    textArea.classList.add("copytextarea");
    textArea.innerText=url;
    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();
    document.execCommand('copy');
    document.body.removeChild(textArea);
  });

  document.getElementById("zoomInButton").addEventListener("click",() => {
    zoomingIn=true;
    canvas.classList.add("zooming");
  });

  document.getElementById("zoomOutButton").addEventListener("click",() => {
    zoomingIn=false;
    canvas.classList.remove("zooming");

    scale = scale/zoomFactor;
    x0 = (x0 + (zoomFactor-1)*(canvas.width / 2))/zoomFactor;
    y0 = (y0 + (zoomFactor-1)*(canvas.height / 2))/zoomFactor;
  });

  document.getElementById("zoomResetButton").addEventListener("click", () =>{
    zoomingIn=false;
    canvas.classList.remove("zooming");
    computeViewport();
  });

  document.getElementById("importButton").addEventListener("click",() => {
    let input = document.getElementById("poseText").value;
    let pose;
    pose = JSON.parse(input);
    setPose(pose);
    //draw();
  });

  document.getElementById("importMovieButton").addEventListener("click",() => {
    let file = document.getElementById("movieFile").files[0];
    let fr = new FileReader();
    fr.onload = receivedMovie;
    fr.readAsText(file);
  });

  document.getElementById("nextFrameButton").addEventListener("click",() => {
    nextFrame();
  });

  document.getElementById("previousFrameButton").addEventListener("click",() => {
    if (frames.length == 0)
      return;
    currentFrame--;
    if (currentFrame < 0)
      currentFrame = frames.length - 1;
    gotoFrame(currentFrame);
  });

  document.getElementById("addFrameButton").addEventListener("click",() => {
    frames.splice(currentFrame + 1, 0, getPose());
    currentFrame++;
    if (currentFrame >= frames.length)
      currentFrame = 0;
    drawFrameCount();
  });

  document.querySelectorAll(".button>input").forEach((txt)=>{
    txt.addEventListener("click",(e)=>{e.stopPropagation()}); //prevent click to propagate to button
  });
  
  function receivedMovie(e) {
    let lines = e.target.result;
    frames = JSON.parse(lines);
    gotoFrame(0);
  }

  function gotoFrame(f) {
    if (f < 0 || f > frames.length)
      return;
    currentFrame = f;
    setPose(frames[currentFrame]);
    //draw();
    drawFrameCount();
  }

  let barycenter = function(){
    let sumX = 0;
    let sumY = 0;
    let count=0;
    for(let i=0; i<figure.vertices.length; i++){
      if(selected[i]){
        sumX += figure.vertices[i][0];
        sumY += figure.vertices[i][1];
        count += 1;
      }
    }
    if(count == 0){
      count = 1;
    }
    let midX = Math.round(sumX/count);
    let midY = Math.round(sumY/count);
    return [midX,midY];
  }

  document.getElementById("flipButton").addEventListener("click",() => {
    let vMid = barycenter();
    for(let i=0; i<figure.vertices.length; i++){
      if(selected[i]){
        figure.vertices[i][0] = 2*vMid[0] - figure.vertices[i][0];
      }
    }
  });

  document.getElementById("rotateButton").addEventListener("click",() => {
    let vMid = barycenter();
    for(let i=0; i<figure.vertices.length; i++){
      if(selected[i]){
        let newX = vMid[0] - vMid[1] + figure.vertices[i][1];
        let newY = vMid[1] + vMid[0] - figure.vertices[i][0];
        figure.vertices[i][0] = newX;
        figure.vertices[i][1] = newY;
      }
    }
  });

  document.getElementById("seekButton").addEventListener("click",()=>{
    if(document.getElementById("seekButton").classList.contains("invalid")){
      return;
    }
    distanceConstraints = [];
    for(let i=0; i<figure.vertices.length; i++){
      distanceConstraints.push([]);
    }
    for(let i=0; i<figure.edges.length; i++){
      let index0 = figure.edges[i][0];
      let index1 = figure.edges[i][1];
      let v0 = figure.vertices[index0];
      let v1 = figure.vertices[index1];

      let v0Orig = initialFigure.vertices[initialFigure.edges[i][0]];
      let v1Orig = initialFigure.vertices[initialFigure.edges[i][1]];
      let dOrig = dSquared(v0Orig, v1Orig);

      distanceConstraints[index0].push({x: v1[0], y: v1[1], d: dOrig});
      distanceConstraints[index1].push({x: v0[0], y: v0[1], d: dOrig});
    }

    let minDist = null;
    let minChoice = null;
    for(let i=0; i<figure.vertices.length; i++){
      if(!movable(i)){
        continue;
      }

      let isGreen = true;
      for(let j=0; j<distanceConstraints[i].length; j++){
        let dxy = dSquared(figure.vertices[i],[distanceConstraints[i][j].x, distanceConstraints[i][j].y]);
        if(Math.abs(dxy-distanceConstraints[i][j].d)*1000000 > epsilon*distanceConstraints[i][j].d){
           isGreen = false;
        }
      }
      if(isGreen){
        continue;
      }
      let minX = figure.vertices[i][0]-searchRadius;
      let maxX = figure.vertices[i][0]+searchRadius;
      let minY = figure.vertices[i][1]-searchRadius;
      let maxY = figure.vertices[i][1]+searchRadius;
      for(let x=minX; x<= maxX; x++){
        for(let y=minY; y<= maxY; y++){
          let goodPoint = true;
          for(let j=0; j<distanceConstraints[i].length; j++){
            let dxy = dSquared([x,y],[distanceConstraints[i][j].x, distanceConstraints[i][j].y]);
            if(Math.abs(dxy-distanceConstraints[i][j].d)*1000000 > epsilon*distanceConstraints[i][j].d){
              goodPoint = false;
            }
          }
          if(goodPoint){
            let dToPt = dSquared([x,y], figure.vertices[i]);
            if(minDist === null || minDist > dToPt){
              minDist = dToPt;
              minChoice = {vertex: i, newVertex: [x,y]};
            }
          }
        }
      }
    }
    if(minChoice !== null){
      console.log("setting " + minChoice.vertex + " to " + minChoice.newVertex);
      figure.vertices[minChoice.vertex] = minChoice.newVertex;
      computeDislikes();
      computeGlobalistStretch();
    }
    else{
      document.getElementById("seekButton").classList.add("invalid");
      console.log("cannot move anything");
    }
  });
}

function setBonusInfo(id) {
  let grantee = [];
  let grants = [];
  for(otherId in problems) {
    let bonuses = [];
    if(problems[otherId].bonuses) {
      bonuses = problems[otherId].bonuses;
      for(let i=0; i<bonuses.length; i++){
        let bonus = bonuses[i];
        if (bonus.problem == id) {
          let bonusCopy = {bonus: bonus.bonus, problem: otherId};
          grantee.push(bonusCopy);
        }
      }
    }
  }
  if(problems[id].bonuses)
    grants = problems[id].bonuses;
  let text = "\n";
  for(let i=0; i<grantee.length; i++) {
    text += grantee[i].bonus + " possibly available from problem " + grantee[i].problem + "\n";
  }
  text += "\n";
  for(let i=0; i<grants.length; i++) {
    text += grants[i].bonus + " obtainable at [" + grants[i].position[0] + ", " +  grants[i].position[1] + "] for problem " + grants[i].problem + "\n";
  }
  document.getElementById("bonusInfo").innerText = text;
}

function recover(newId, figure){
  id = newId;
  setBonusInfo(id);
  load(problems[id].hole, problems[id].bonuses, problems[id].figure, figure, problems[id].epsilon);
}

populateProblemrow();
let params = new URLSearchParams(window.location.search)

if(params.has('id')){
  id = params.get('id');
  setBonusInfo(id);
}
if(params.has('figure')){
  let fig = JSON.parse(atob(params.get('figure')));
  load(problems[id].hole, problems[id].bonuses, fig.initial, fig.current, problems[id].epsilon);
}
else{
  load(problems[id].hole, problems[id].bonuses, problems[id].figure, problems[id].figure, problems[id].epsilon);
}

