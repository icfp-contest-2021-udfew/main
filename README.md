# Team Udfew's Code for ICFP Contest 2021

## Description

We produced solutions
- manually, with [tool assistance](#visualiser),
- with an [automated solver](#optimizer).

A combination of both methods was possible
- by providing a manually crafted pose to the optimizer annd
- by loading the [sequence of optimization steps into the visualiser](#movies).

Among all produced solutions a [score-maximizing selection is computed](#selecting-solutions) which guarantees that every used bonus is acquired.

## Components

### Visualiser

#### Usage

This tool consists solely of a [static website](www/visualiser/index.html).
It expects a [problems file](www/visualiser/problems.hs) which is generated automatically with `./download problems`.

![](visualiser.png)

#### Basic Controls

The main functionality is to manipulate poses with drag-and-drop controls.

Edges are rendered green if they satisfy their length constraint, dark red if they are understretched, bright red if they are overstretched.

Holding CTRL enables box selecting multiple vertices, these are then moved without changing their relative position.

Hole constraints are not checked automatically.

#### Buttons

* Export/Import Solution: 
The current pose can be exported to, and imported from, a textbox below, using the export and import buttons.
This can also be used as a quicksave feature.

* Save to clipboard: Generates a URL that stores the current pose. Doesn't work with very large problems.

* ZoomIn/Out/Reset: can be used to control the viewport.

* Flip and Rotate: adjust the selected vertices only. This is useful for folding figures in certain problems. Using these two buttons, it is possible to achieve all symmetries of the lattice (rotations by multiples of 90 degrees, mirroring along either of the two axes or two diagonals).

* Clipping and Snapping: affect how vertices are pulled. Clipping constrains this in such a way that no green edges are destroyed (and is largely superseded by "Show valid pts", see below), Snapping lets vertices snap to hole vertices or bonuses.

* Show Hole Pts: Renders hole vertices as large red circles (useful for debugging attempts at 0 dislike solutions)

* Find Hole Path: Once activated, this allows the user to select a contiguous segment of hole edges. The resulting path is matched against the figure graph. If there is only one match left, it is automatically moved into position. If Snapping is active, it snaps (affecting annealing etc.Find Hole Path: Once activated, this allows the user to select a contiguous segment of hole edges. The resulting path is matched against the figure graph. If there is only one match left, it is automatically moved into position. If Snapping is active, it snaps (affecting Annealing etc.) Small bug: This feature doesn't exclude self-intersecting paths, which lead to problems.

* Tooltips: When active, hovering over edges and vertices displays information.

* Show valid points: When active, constraints of currently dragged vertex are visualised.

* Seek valid points: This tries to move points which don't yet satisfy all distance constraints in such a way that they do. It moves a single point, namely the one that needs to be moved the smallest distance.

* Annealing: When active, all edges behave as springs, which try to contract to their optimal length. Works "live": Vertices can still be dragged around, to try and push the figure into the desired shape. Coordinates are floating point numbers. When turned off again, all coordinates are rounded to the nearest integer point. If Snapping is active, Annealing doesn't move vertices which have been snapped.

* Keep Annealing: When not active, Annealing turns off automatically once all edges are green (and then rounds everything to integer coordinates).

* AutoRound: While active, rounds everything to integer vertices once a second (adjustible with Frequency input). Useful to see how good the currently annealed pose actually is. (Bump is an experimental feature to randomize the rounding, it didn't turn out to be that useful).

* Noise: Experimental feature to randomize the physics. Also didn't turn out to be that useful.

* Globalist/Break a leg/Wallhack/Superflex: Activate the corresponding bonus. Superflex is factored into the physics of Annealing. (So is Break a leg, since it simply modifies the figure)

#### Movies

A movie is an array of poses, called frames. It is possible to import movie files in json format. This was used as a debugging feature for the solver.
It is also possible to use movies as a simple saving method for multiple attempts.

* Import movie / Export movie: load a movie file / export the movie in json format 

* Add current figure: inserts the current figure as a frame into the movie

* Play: plays the movie. The number field in the button is the is refresh period of the playback

* Previous / Next frame: single stepping through frames

### Optimizer

The `optimize` binary is a(n admittedly hacky) "driver" for various solvers
and optimizers implementing the following traits:

* `Optimizer`: Tries to optimize the given pose for a given problem.
Implemented by:
  - "gradient descent"-type optimizers which try to minimize the output of a
`Scorer`
  - an "annealer" that simulates edges as springs
* `Solver`: Tries to find a solution for a given problem.
All of the current implementers transform the initial pose in some way and then
use an `Optimizer`.
Such transformations include:
  - rotating the pose
  - moving vertices to random positions
* `Scorer`: Assigns a score to a pose, where smaller is better.
The output should implement `PartialOrd` (and several other basic traits).
`Scorer`s can be combined, for example to use the lexicographic order
on tuples of scores.
Implementations include:
  - dislikes
  - how much the edges are "overstretched"
  - sum of the distances of the pose vertices to the hole
  - area cut out by the hole and the figure segments that are not inside the
hole
* `MoveFactory`: Produces an iterator of possible moves for a given pose.
Implementations include:
  - vertex translations
  - vertex jumps to "valid" positions
  - random vertex translations
  - pose translations
  - pose rotations

#### Usage

This binary is "self-documenting", please see the output of
```
cargo run --release --bin optimize -- --help
```

(We are refraining from giving more details here in order to minimize the risk
of having out of date documentation when the binary changes.)

### Downloading Problems and Scoreboard

#### Usage

Call `./download [scoreboard] [problems]` to download the current scoreboard
or update the problem files.

#### Dependencies

- curl
- perl
- xmllint

### Analysing Problems

This script is used to update our problems wiki

#### Usage

`./analyse_problem_score`

#### Dependencies

- php
- perl
- xmllint

### Selecting Solutions

A score-optimising bonus-valid combination of solutions is computed via a dynamic programming algorithm. All solutions in the [solution folder](data/solutions) are considered.

#### Usage

The currently _selected_ solution-combination is stored in [solutions.json](solutions.json).

The currently _submitted_ solution-combination is stored in [submissions.json](submissions.json).

To update `solutions.json`, call `./select_solutions`.
If you want to get the most accurate result, make sure the scoreboard is up to date.

To submit the selected solutions, call `./submit`.

#### Dependencies

- php
- perl
- xmllint

### Validation

#### Usage

To validate pose files, call:

`./validate_solutions [PoseFile(s)]`

This is a wrapper for the rust binary `check`, which is executed by calling

```
cargo run --release --bin check -- $ARGS [PROBLEM_FILE] [POSE_FILE]
```

within the `rust` directory.

**Exit code:** `0` if the checker thinks that the solution is valid, `1` otherwise.

**Output:** If  there are errors, they are printed to `stderr`.

## License

Original content in this repository is available under the GNU General Public
License version 3 (see `LICENSE.txt`) or later.

The repository may contain third party content that is licensed differently.
