default: all

all: rust

.PHONY: rust
rust:
	cd rust && $(MAKE)
